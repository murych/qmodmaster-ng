include(FetchContent)
set(CMAKE_TLS_VERIFY true)

# Avoid warning about DOWNLOAD_EXTRACT_TIMESTAMP in CMake 3.24:
if(CMAKE_VERSION VERSION_GREATER_EQUAL "3.24.0")
  cmake_policy(SET CMP0135 NEW)
endif()

FetchContent_Declare(
  QtAwesome
  GIT_REPOSITORY https://github.com/gamecreature/QtAwesome.git
  GIT_TAG font-awesome-6.3.0)
FetchContent_MakeAvailable(QtAwesome)
