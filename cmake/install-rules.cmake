install(
    TARGETS qmodmaster-ng_exe
    RUNTIME COMPONENT qmodmaster-ng_Runtime
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
