#ifndef BUSMONITOR_H
#define BUSMONITOR_H

#include "models/rawdatamodel.h"
#include <QLabel>
#include <QMainWindow>
#include <QPlainTextEdit>
#include <gsl-lite/gsl-lite.hpp>

namespace Ui {
class BusMonitor;
}

class BusMonitor : public QMainWindow
{
  Q_OBJECT

public:
  explicit BusMonitor(RawDataModel* raw_data_model, QWidget* parent = nullptr);
  ~BusMonitor() override;

private:
  std::unique_ptr<Ui::BusMonitor> m_ui{ nullptr };
  RawDataModel* m_rawDataModel{ nullptr };

  void parseTxMsg(const QString& msg, QPlainTextEdit* txt_adu);
  static void parseTxPDU(const QStringList& pdu,
                         const QString& slave,
                         QPlainTextEdit* txtADU);
  void parseRxMsg(const QString& msg, QPlainTextEdit* txt_adu);
  static void parseRxPDU(const QStringList& pdu,
                         const QString& slave,
                         QPlainTextEdit* txtADU);
  static void parseSysMsg(const QString& msg, QPlainTextEdit* txtADU);

protected:
  void closeEvent(QCloseEvent* event) override;
  void showEvent(QShowEvent* event) override;

private slots:
  void clear();
  void exit();
  void save();
  void SxS();
  void selectedRow(const QModelIndex& selected);
};

#endif // BUSMONITOR_H
