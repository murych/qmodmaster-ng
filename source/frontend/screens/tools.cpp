#include "tools.h"
#include "ui_tools.h"

#include "utils/eutils.h"

Tools::Tools(ModbusAdapter* adapter,
             ModbusCommSettings* settings,
             QWidget* parent)
  : QMainWindow{ parent }
  , m_modbusAdapter{ adapter }
  , m_modbusCommSettings{ settings }
  , m_ui{ std::make_unique<Ui::Tools>() }
  , m_cmbModbusMode{ std::make_unique<QComboBox>(this) }
  , m_cmbCmd{ std::make_unique<QComboBox>(this) }
{
  qDebug() << this << "CONSTRUCTOR";

  // setup UI
  m_ui->setupUi(this);

  m_cmbModbusMode->setMinimumWidth(96);
  m_cmbModbusMode->addItem("RTU/TCP");
  m_cmbModbusMode->addItem("TCP");

  m_cmbCmd->setMinimumWidth(96);
  m_cmbCmd->addItem("Report Slave ID");

  m_ui->toolBar->addWidget(m_cmbModbusMode.get());
  m_ui->toolBar->addWidget(m_cmbCmd.get());
  m_ui->toolBar->addSeparator();
  m_ui->toolBar->addAction(m_ui->actionExec);
  m_ui->toolBar->addAction(m_ui->actionClear);
  m_ui->toolBar->addAction(m_ui->actionExit);

  // UI - connections
  connect(m_cmbModbusMode.get(),
          &QComboBox::currentIndexChanged,
          this,
          &Tools::changedModbusMode);

  connect(m_ui->actionExec, &QAction::triggered, this, &Tools::execCmd);
  connect(m_ui->actionClear, &QAction::triggered, this, &Tools::clear);
  connect(m_ui->actionExit, &QAction::triggered, this, &Tools::exit);
  connect(
    &m_pingProc, &QProcess::readyReadStandardOutput, this, &Tools::pingData);
  connect(
    &m_pingProc, &QProcess::readyReadStandardError, this, &Tools::pingData);
}

Tools::~Tools() = default;

void
Tools::exit()
{
  close();
}

QString
Tools::ipConv(const QString& ip)
{
  /* convert ip - remove leding 0's */

  // QStringList m_ip;
  QStringList m_ip_conv;
  QString m_ip_byte;

  const auto m_ip = ip.split(".");
  for (const auto& i : m_ip) {
    m_ip_byte = i;
    if (m_ip_byte.at(0) == '0') {
      m_ip_conv << m_ip_byte.remove(0, 1);
    } else {
      m_ip_conv << m_ip_byte;
    }
  }

  return (m_ip_conv.at(0) + "." + m_ip_conv.at(1) + "." + m_ip_conv.at(2) +
          "." + m_ip_conv.at(3));
}

void
Tools::changedModbusMode(int curr_index)
{
  qDebug() << "Modbus Mode changed. Index = " << curr_index;

  m_cmbCmd->clear();
  if (curr_index == 0) { // RTU/TCP
    m_cmbCmd->addItem("Report Slave ID");
  } else { // TCP
    m_cmbCmd->addItem("Report Slave ID");
    m_cmbCmd->addItem("Ping");
    m_cmbCmd->addItem("Port Status");
  }
}

void
Tools::execCmd()
{
  m_ui->txtOutput->moveCursor(QTextCursor::End);

  qDebug() << "Tools Execute Cmd " << m_cmbCmd->currentText();
  switch (m_cmbCmd->currentIndex()) {
    case 0:
      m_ui->txtOutput->appendPlainText(
        QString("------- Modbus Diagnotics : Report Slave ID %1 -------\n")
          .arg(m_modbusCommSettings->slaveID()));
      diagnosticsProc();
      break;

    case 1:
      m_ui->txtOutput->appendPlainText(
        QString("------- Modbus TCP : Ping IP %1 -------\n")
          .arg(m_modbusCommSettings->slaveIP()));
      pingProc();
      break;

    case 2:
      m_ui->txtOutput->appendPlainText(
        QString("------- Modbus TCP : Check Port %1:%2 Status -------\n")
          .arg(m_modbusCommSettings->slaveIP(),
               m_modbusCommSettings->tcpPort()));
      portProc();
      break;

    default:
      m_ui->txtOutput->appendPlainText("------- No Valid Selection -------\n");
      break;
  }
}

void
Tools::clear()
{
  qDebug() << "Tools Clear Ouput";
  m_ui->txtOutput->clear();
}

void
Tools::diagnosticsProc()
{
  qApp->processEvents();
  m_ui->txtOutput->moveCursor(QTextCursor::End);
  if (m_modbusAdapter->m_modbus != nullptr) {
    modbusDiagnostics();
  } else {
    m_ui->txtOutput->insertPlainText("Not Connected.\n");
  }
}

void
Tools::pingProc()
{
  qApp->processEvents();
  m_pingProc.start("ping",
                   QStringList() << ipConv(m_modbusCommSettings->slaveIP()));
  if (m_pingProc.waitForFinished(5000)) {
    // just wait -> execute button is pressed
  }
}

void
Tools::pingData()
{
  qApp->processEvents();
  m_ui->txtOutput->moveCursor(QTextCursor::End);
  m_ui->txtOutput->insertPlainText(m_pingProc.readAll());
}

void
Tools::portProc()
{
  qApp->processEvents();
  m_ui->txtOutput->moveCursor(QTextCursor::End);
  m_portProc.connectToHost(ipConv(m_modbusCommSettings->slaveIP()),
                           m_modbusCommSettings->tcpPort().toInt());
  if (m_portProc.waitForConnected(5000)) { // wait -> execute button is pressed
    m_ui->txtOutput->insertPlainText("Connected.Port is opened\n");
    m_portProc.close();
  } else {
    m_ui->txtOutput->insertPlainText("Not connected.Port is closed\n");
  }
}

void
Tools::modbusDiagnostics()
{
  // Modbus diagnostics - RTU/TCP
  qDebug() << "Modbus diagnostics.";

  // Modbus data
  m_modbusAdapter->setFunctionCode(0x11);
  uint8_t dest[1024]; // setup memory for data
  memset(dest, 0, 1024);
  int ret = -1; // return value from read functions

  modbus_set_slave(m_modbusAdapter->m_modbus, m_modbusCommSettings->slaveID());
  // request data from modbus
  ret = modbus_report_slave_id(
    m_modbusAdapter->m_modbus, MODBUS_MAX_PDU_LENGTH, dest);
  qDebug() << "Modbus Read Data return value = " << ret
               << ", errno = " << errno;

  // update data model
  if (ret > 1) {
    QString line;
    line = dest[1] != 0u ? "ON" : "OFF";
    m_ui->txtOutput->insertPlainText("Run Status : " + line + "\n");
    QString id = QString::fromUtf8((char*)dest);
    m_ui->txtOutput->insertPlainText("ID : " + id.right(id.size() - 2) + "\n");
    ;
  } else {
    QString line = "";
    if (ret < 0) {
      line = QString("Error : ") + EUtils::libmodbus_strerror(errno);
      qWarning() << "Read diagnostics data failed. " << line;
      line = QString(tr("Read diagnostics data failed.\nError : ")) +
             EUtils::libmodbus_strerror(errno);
      m_ui->txtOutput->insertPlainText(line);
    } else {
      line = QString("Unknown Error : ") + EUtils::libmodbus_strerror(errno);
      qWarning() << "Read diagnostics data failed. " << line;
      line = QString(tr("Read diagnostics data failed.\nUnknown Error : ")) +
             EUtils::libmodbus_strerror(errno);
      m_ui->txtOutput->insertPlainText(line);
    }

    modbus_flush(m_modbusAdapter->m_modbus); // flush data
  }
}
