#include "about.h"
#include "ui_about.h"

#include "modbus-version.h"

About::About(QWidget* parent)
  : QDialog{ parent }
  , m_ui{ std::make_unique<Ui::About>() }
{
  qDebug() << this << "CONSTRUCTOR";

  m_ui->setupUi(this);
  m_ui->lblVersion->setText(QStringLiteral("QModMaster 0.5.3-beta"));
  m_ui->lblLibVersion->setText(
    QStringLiteral("libmodbus %1").arg(LIBMODBUS_VERSION_STRING));
  m_ui->lblURL->setText(
    QStringLiteral("<a href = "
                   "http://sourceforge.net/projects/qmodmaster"
                   ">Sourceforge Project Home Page</a>"));
}

About::~About() = default;
