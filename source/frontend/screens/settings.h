#ifndef SETTINGS_H
#define SETTINGS_H

#include "settings/modbuscommsettings.h"
#include <QDialog>
#include <QSettings>
#include <gsl-lite/gsl-lite.hpp>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
  Q_OBJECT

public:
  explicit Settings(ModbusCommSettings* settings, QWidget* parent = nullptr);
  ~Settings() override;

  bool modbusConnected{ false };

private:
  std::unique_ptr<Ui::Settings> m_ui{ nullptr };
  gsl::owner<ModbusCommSettings*> m_settings{ nullptr };

signals:
  void changedEndianess(int endian);

private slots:
  void changesAccepted();

protected:
  void showEvent(QShowEvent* event) override;
};

#endif // SETTINGS_H
