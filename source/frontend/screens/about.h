#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>

namespace Ui {
class About;
}

class About : public QDialog
{
  Q_OBJECT

public:
  explicit About(QWidget* parent = nullptr);
  ~About() override;

private:
  std::unique_ptr<Ui::About> m_ui{ nullptr };
};

#endif // ABOUT_H
