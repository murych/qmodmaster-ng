#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "utils/eutils.h"
#include <QCoreApplication>
#include <QDesktopServices>
#include <QFileDialog>
#include <QMessageBox>
#include <QTranslator>
#include <QUrl>

MainWindow* main_win;

MainWindow::MainWindow(ModbusAdapter* adapter,
                       ModbusCommSettings* settings,
                       QWidget* parent)
  : QMainWindow{ parent }
  , m_modbus{ adapter }
  , m_modbusCommSettings{ settings }
  , m_ui{ std::make_unique<Ui::MainWindow>() }
  , m_dlgAbout{ std::make_unique<About>() }
  , m_dlgModbusRTU{ std::make_unique<SettingsModbusRTU>(m_modbusCommSettings,
                                                        this) }
  , m_dlgModbusTCP{ std::make_unique<SettingsModbusTCP>(m_modbusCommSettings,
                                                        this) }
  , m_dlgSettings{ std::make_unique<Settings>(m_modbusCommSettings, this) }
  , m_busMonitor{ std::make_unique<BusMonitor>(adapter->rawModel.get(), this) }
  , m_tools{ std::make_unique<Tools>(adapter, settings, this) }
{

  // setup UI
  m_ui->setupUi(this);
  m_ui->sbNoOfRegs->setEnabled(true);
  m_ui->actionRead_Write->setEnabled(false);
  m_ui->actionScan->setEnabled(false);
  m_ui->sbStartAddress->setMinimum(m_modbusCommSettings->baseAddr().toInt());
  m_ui->cmbFrmt->setCurrentIndex(m_modbusCommSettings->frmt());
  m_ui->cmbFunctionCode->setCurrentIndex(m_modbusCommSettings->functionCode());
  m_ui->cmbModbusMode->setCurrentIndex(m_modbusCommSettings->modbusMode());
  m_ui->sbSlaveID->setValue(m_modbusCommSettings->slaveID());
  m_ui->spInterval->setValue(m_modbusCommSettings->scanRate());
  m_ui->sbStartAddress->setValue(m_modbusCommSettings->startAddr());
  m_ui->sbNoOfRegs->setValue(m_modbusCommSettings->noOfRegs());
  m_ui->chkSigned->setVisible(false);
  m_ui->tblRegisters->setStyleSheet(
    "QHeaderView::section { background-color:lightGray }");

  // UI - dialogs
  connect(
    m_ui->actionAbout, &QAction::triggered, m_dlgAbout.get(), &QDialog::show);

  connect(m_ui->actionSerial_RTU,
          &QAction::triggered,
          this,
          &MainWindow::showSettingsModbusRTU);

  connect(m_ui->actionTCP,
          &QAction::triggered,
          this,
          &MainWindow::showSettingsModbusTCP);

  connect(
    m_ui->actionSettings, &QAction::triggered, this, &MainWindow::showSettings);

  connect(m_ui->actionBus_Monitor,
          &QAction::triggered,
          this,
          &MainWindow::showBusMonitor);

  connect(m_ui->actionTools, &QAction::triggered, this, &MainWindow::showTools);

  // UI - connections
  connect(m_ui->cmbModbusMode,
          &QComboBox::currentIndexChanged,
          this,
          &MainWindow::changedModbusMode);
  connect(m_ui->cmbFunctionCode,
          &QComboBox::currentIndexChanged,
          this,
          &MainWindow::changedFunctionCode);
  connect(m_ui->cmbFrmt,
          &QComboBox::currentIndexChanged,
          this,
          &MainWindow::changedFrmt);
  connect(
    m_ui->chkSigned, &QCheckBox::toggled, this, &MainWindow::changedDecSign);
  connect(m_ui->cmbStartAddrBase,
          &QComboBox::currentIndexChanged,
          this,
          &MainWindow::changedStartAddrBase);
  connect(m_ui->sbSlaveID,
          &QSpinBox::valueChanged,
          this,
          &MainWindow::changedSlaveID);
  connect(m_ui->sbNoOfRegs,
          &QSpinBox::valueChanged,
          this,
          &MainWindow::changedNoOfRegs);
  connect(m_ui->sbStartAddress,
          &QSpinBox::valueChanged,
          this,
          &MainWindow::changedStartAddress);
  connect(m_ui->spInterval,
          &QSpinBox::valueChanged,
          this,
          &MainWindow::changedScanRate);
  connect(m_ui->sbPrecision,
          &QSpinBox::valueChanged,
          this,
          &MainWindow::changedFloatPrecision);
  connect(
    m_ui->actionClear, &QAction::triggered, this, &MainWindow::clearItems);
  connect(m_ui->actionRead_Write,
          &QAction::triggered,
          this,
          &MainWindow::modbusRequest);
  connect(
    m_ui->actionScan, &QAction::toggled, this, &MainWindow::modbusScanCycle);
  connect(
    m_ui->actionConnect, &QAction::toggled, this, &MainWindow::changedConnect);
  connect(m_ui->actionReset_Counters,
          &QAction::triggered,
          this,
          &MainWindow::resetCounters);
  connect(m_ui->actionOpenLogFile,
          &QAction::triggered,
          this,
          &MainWindow::openLogFile);
  connect(
    m_ui->actionHeaders, &QAction::triggered, this, &MainWindow::showHeaders);
  connect(m_ui->actionModbus_Manual,
          &QAction::triggered,
          this,
          &MainWindow::openModbusManual);
  // connect(m_ui->actionEnglish_en_US,SIGNAL(triggered()),this,SLOT(changeLanguage()));
  // connect(m_ui->actionSimplified_Chinese_zh_CN,SIGNAL(triggered()),this,SLOT(changeLanguage()));
  // connect(m_ui->actionTraditional_Chinese_zh_TW,SIGNAL(triggered()),this,SLOT(changeLanguage()));
  connect(m_ui->actionLoad_Session,
          &QAction::triggered,
          this,
          &MainWindow::loadSession);
  connect(m_ui->actionSave_Session,
          &QAction::triggered,
          this,
          &MainWindow::saveSession);
  connect(m_dlgSettings.get(),
          &Settings::changedEndianess,
          this,
          &MainWindow::changedEndianess);

  // UI - status
  m_statusInd = new QLabel;
  m_statusInd->setFixedSize(16, 16);
  m_statusText    = new QLabel;
  m_baseAddr      = new QLabel(tr("Base Addr : ") + "0");
  m_statusPackets = new QLabel(tr("Packets : ") + "0");
  m_statusPackets->setStyleSheet("QLabel {color:blue;}");
  m_statusErrors = new QLabel(tr("Errors : ") + "0");
  m_statusErrors->setStyleSheet("QLabel {color:red;}");
  m_endian = new QLabel(tr("Endian : ") +
                        EUtils::endianness(m_modbusCommSettings->endian()));
  m_endian->setStyleSheet("QLabel {color:black;}");
  m_ui->statusBar->addWidget(m_statusInd);
  m_ui->statusBar->addWidget(m_statusText, 10);
  m_ui->statusBar->addWidget(m_baseAddr, 10);
  m_ui->statusBar->addWidget(m_statusPackets, 10);
  m_ui->statusBar->addWidget(m_endian, 10);
  m_ui->statusBar->addWidget(m_statusErrors, 10);
  m_statusInd->setPixmap(QPixmap(":/img/ballorange-16.png"));

  // Setup Toolbar
  m_ui->mainToolBar->addAction(m_ui->actionLoad_Session);
  m_ui->mainToolBar->addAction(m_ui->actionSave_Session);
  m_ui->mainToolBar->addAction(m_ui->actionConnect);
  m_ui->mainToolBar->addAction(m_ui->actionRead_Write);
  m_ui->mainToolBar->addAction(m_ui->actionScan);
  m_ui->mainToolBar->addAction(m_ui->actionClear);
  m_ui->mainToolBar->addAction(m_ui->actionReset_Counters);
  m_ui->mainToolBar->addSeparator();
  m_ui->mainToolBar->addAction(m_ui->actionOpenLogFile);
  m_ui->mainToolBar->addAction(m_ui->actionBus_Monitor);
  m_ui->mainToolBar->addAction(m_ui->actionTools);
  m_ui->mainToolBar->addAction(m_ui->actionHeaders);
  m_ui->mainToolBar->addSeparator();
  m_ui->mainToolBar->addAction(m_ui->actionSerial_RTU);
  m_ui->mainToolBar->addAction(m_ui->actionTCP);
  m_ui->mainToolBar->addAction(m_ui->actionSettings);
  m_ui->mainToolBar->addSeparator();
  m_ui->mainToolBar->addAction(m_ui->actionModbus_Manual);
  m_ui->mainToolBar->addAction(m_ui->actionAbout);
  m_ui->mainToolBar->addAction(m_ui->actionExit);

  // Init models
  m_ui->tblRegisters->setItemDelegate(m_modbus->regModel->itemDelegate());
  m_ui->tblRegisters->setModel(m_modbus->regModel->model.get());
  m_ui->tblRegisters->horizontalHeader()->hide();
  m_ui->tblRegisters->verticalHeader()->hide();
  changedFrmt(m_modbusCommSettings->frmt());
  m_modbus->regModel->setStartAddrBase(10);
  m_modbus->regModel->setEndian(m_modbusCommSettings->endian());
  m_modbus->setBaseAddr(m_modbusCommSettings->baseAddr().toInt());
  m_modbus->regModel->setFloatPrecision(m_modbusCommSettings->floatPrecision());
  m_modbus->setReadOutputsBeforeWrite(
    m_modbusCommSettings->readOutputsBeforeWrite());
  clearItems(); // init model ui

  // Update UI
  changedFunctionCode(m_modbusCommSettings->functionCode());
  changedModbusMode(m_modbusCommSettings->modbusMode());
  updateStatusBar();
  refreshView();

  // Logging level
  qInfo() << "Start Program";
}

MainWindow::~MainWindow()
{
  if (m_modbus != nullptr) {
    m_modbus->modbusDisConnect();
  }

  qInfo() << "Stop Program";
}

void
MainWindow::showSettingsModbusRTU()
{
  // Show RTU Settings Dialog
  m_dlgModbusRTU->modbusConnected = m_modbus->isConnected();
  if (m_dlgModbusRTU->exec() == QDialog::Accepted) {
    qDebug() << "RTU settings changes accepted ";
    updateStatusBar();
    m_modbusCommSettings->saveSettings();
  } else
    qWarning() << "RTU settings changes rejected ";
}

void
MainWindow::showSettingsModbusTCP()
{
  // Show TCP Settings Dialog
  m_dlgModbusTCP->modbusConnected = m_modbus->isConnected();
  if (m_dlgModbusTCP->exec() == QDialog::Accepted) {
    qDebug() << "TCP settings changes accepted ";
    updateStatusBar();
    m_modbusCommSettings->saveSettings();
  } else
    qWarning() << "TCP settings changes rejected ";
}

void
MainWindow::showSettings()
{
  // Show General Settings Dialog
  m_dlgSettings->modbusConnected = m_modbus->isConnected();

  const auto ret{ m_dlgSettings->exec() };
  qDebug() << ret;

  if (ret == QDialog::Accepted) {
    qDebug() << "Settings changes accepted ";
    m_modbus->rawModel->setMaxNoOfLines(
      m_modbusCommSettings->maxNoOfLines().toInt());
    m_modbus->setTimeOut(m_modbusCommSettings->timeOut().toInt());
    m_ui->sbStartAddress->setMinimum(m_modbusCommSettings->baseAddr().toInt());
    m_modbus->setBaseAddr(m_modbusCommSettings->baseAddr().toInt());
    m_modbus->regModel->setEndian(m_modbusCommSettings->endian());
    m_modbusCommSettings->saveSettings();
  } else
    qWarning() << "Settings changes rejected ";

  updateStatusBar();
}

void
MainWindow::showBusMonitor()
{
  // Show Bus Monitor

  m_modbus->rawModel->setMaxNoOfLines(
    m_modbusCommSettings->maxNoOfLines().toInt());
  m_busMonitor->move(this->x() + this->width() + 20, this->y());
  m_busMonitor->show();
}

void
MainWindow::showTools()
{
  // Show Tools

  m_tools->move(this->x() + this->width() + 40, this->y() + 20);
  m_tools->show();
}

void
MainWindow::changedModbusMode(int curr_index)
{
  // Change lblSlave text : Slave Addr, Unit ID

  qDebug() << "Modbus Mode changed. Index = " << curr_index;
  m_modbusCommSettings->setModbusMode(curr_index);
  m_modbusCommSettings->saveSettings();

  if (curr_index == 0) { // RTU
    m_ui->lblSlave->setText("Slave Addr");
  } else { // TCP
    m_ui->lblSlave->setText("Unit ID");
  }

  updateStatusBar();
}

void
MainWindow::changedFunctionCode(int curr_index)
{
  // Enable-Disable number of coils or registers

  qDebug() << "Function Code changed. Index = " << curr_index;
  m_modbusCommSettings->setFunctionCode(curr_index);
  m_modbusCommSettings->saveSettings();

  const auto function_code{ EUtils::modbusFunctionCode(curr_index) };
  const auto string_number_of_coils{ tr("Number of Coils") };
  const auto string_number_of_inputs{ tr("Number of Inputs") };
  const auto string_number_of_registers{ tr("Number of Registers") };
  switch (function_code) // Label = Read Request, Write Request
  {
    case MODBUS_FC_READ_COILS:
      m_modbus->regModel->setIs16Bit(false);
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(2000);
      m_ui->lblNoOfCoils->setText(string_number_of_coils);
      m_ui->cmbFrmt->setCurrentIndex(0);
      m_ui->cmbFrmt->setEnabled(false);
      break;
    case MODBUS_FC_READ_DISCRETE_INPUTS:
      m_modbus->regModel->setIs16Bit(false);
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(2000);
      m_ui->lblNoOfCoils->setText(string_number_of_inputs);
      m_ui->cmbFrmt->setCurrentIndex(0);
      m_ui->cmbFrmt->setEnabled(false);
      break;
    case MODBUS_FC_READ_HOLDING_REGISTERS:
      m_modbus->regModel->setIs16Bit(true);
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(125);
      m_ui->lblNoOfCoils->setText(string_number_of_registers);
      m_ui->cmbFrmt->setEnabled(true);
      break;
    case MODBUS_FC_READ_INPUT_REGISTERS:
      m_modbus->regModel->setIs16Bit(true);
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(125);
      m_ui->lblNoOfCoils->setText(string_number_of_registers);
      m_ui->cmbFrmt->setEnabled(true);
      break;
    case MODBUS_FC_WRITE_SINGLE_COIL:
      m_modbus->regModel->setIs16Bit(false);
      m_ui->sbNoOfRegs->setValue(1);
      m_ui->sbNoOfRegs->setEnabled(false);
      m_ui->lblNoOfCoils->setText(string_number_of_coils);
      m_ui->cmbFrmt->setCurrentIndex(0);
      m_ui->cmbFrmt->setEnabled(false);
      break;
    case MODBUS_FC_WRITE_MULTIPLE_COILS:
      m_modbus->regModel->setIs16Bit(false);
      if (m_ui->sbNoOfRegs->value() < 2) {
        m_ui->sbNoOfRegs->setValue(2);
      }
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(2000);
      m_ui->lblNoOfCoils->setText(string_number_of_coils);
      m_ui->cmbFrmt->setCurrentIndex(0);
      m_ui->cmbFrmt->setEnabled(false);
      break;
    case MODBUS_FC_WRITE_SINGLE_REGISTER:
      m_modbus->regModel->setIs16Bit(true);
      m_ui->sbNoOfRegs->setValue(1);
      m_ui->sbNoOfRegs->setEnabled(false);
      m_ui->lblNoOfCoils->setText(string_number_of_registers);
      m_ui->cmbFrmt->setEnabled(true);
      break;
    case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
      m_modbus->regModel->setIs16Bit(true);
      if (m_ui->sbNoOfRegs->value() < 2) {
        m_ui->sbNoOfRegs->setValue(2);
      }
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(125);
      m_ui->lblNoOfCoils->setText(string_number_of_registers);
      m_ui->cmbFrmt->setEnabled(true);
      break;
    default:
      m_modbus->regModel->setIs16Bit(false);
      m_ui->sbNoOfRegs->setValue(1);
      m_ui->sbNoOfRegs->setEnabled(true);
      m_ui->sbNoOfRegs->setMaximum(2000);
      m_ui->lblNoOfCoils->setText(string_number_of_coils);
      m_ui->cmbFrmt->setEnabled(true);
      break;
  }

  m_modbus->setNumOfRegs(m_ui->sbNoOfRegs->value());
  addItems();
}

void
MainWindow::changedFrmt(int curr_index)
{
  // Change Format

  qDebug() << "Format changed. Index = " << curr_index;
  m_modbusCommSettings->setFrmt(curr_index);
  m_modbusCommSettings->saveSettings();

  switch (curr_index) {
    case 0:
      m_ui->chkSigned->setVisible(false);
      m_ui->chkSigned->setChecked(false);
      m_modbus->regModel->setFrmt(EUtils::Bin);
      m_ui->lblPrecision->setVisible(false);
      m_ui->sbPrecision->setVisible(false);
      m_ui->sbNoOfRegs->setMinimum(1);
      break;
    case 1:
      m_ui->chkSigned->setVisible(true);
      m_modbus->regModel->setFrmt(EUtils::Dec);
      m_ui->lblPrecision->setVisible(false);
      m_ui->sbPrecision->setVisible(false);
      m_ui->sbNoOfRegs->setMinimum(1);
      break;
    case 2:
      m_ui->chkSigned->setVisible(false);
      m_ui->chkSigned->setChecked(false);
      m_modbus->regModel->setFrmt(EUtils::Hex);
      m_ui->lblPrecision->setVisible(false);
      m_ui->sbPrecision->setVisible(false);
      m_ui->sbNoOfRegs->setMinimum(1);
      break;
    case 3:
      m_ui->chkSigned->setVisible(false);
      m_ui->chkSigned->setChecked(false);
      m_modbus->regModel->setFrmt(EUtils::Float);
      m_ui->lblPrecision->setVisible(true);
      m_ui->sbPrecision->setVisible(true);
      m_ui->sbNoOfRegs->setMinimum(1);
      break;
    default:
      m_modbus->regModel->setFrmt(EUtils::Dec);
      m_ui->chkSigned->setVisible(true);
      m_ui->lblPrecision->setVisible(false);
      m_ui->sbPrecision->setVisible(false);
      m_ui->sbNoOfRegs->setMinimum(1);
      break;
  }

  // clear table on format change
  addItems();
}

void
MainWindow::changedDecSign(bool value)
{
  // Change Dec Signed - Unsigned

  qDebug() << "Dec Signed-Unsigned changed. Signed = " << value;

  m_modbus->regModel->setIsSigned(value);
}

void
MainWindow::changedScanRate(int value)
{
  // Enable-Disable Time Interval

  qDebug() << "ScanRate changed. Value = " << value;
  m_modbusCommSettings->setScanRate(value);
  m_modbusCommSettings->saveSettings();

  m_modbus->setScanRate(value);
}

void
MainWindow::changedConnect(bool value)
{
  // Connect - Disconnect

  if (value) { // Connected
    modbusConnect(true);
    qInfo() << "Connected ";
  } else { // Disconnected
    modbusConnect(false);
    qInfo() << "Disconnected ";
  }

  m_modbus->resetCounters();
  refreshView();
}

void
MainWindow::changedSlaveID(int value)
{
  // Slave ID

  qDebug() << "Slave ID Changed. Value = " << value;
  m_modbusCommSettings->setSlaveID(value);
  m_modbusCommSettings->saveSettings();
}

void
MainWindow::changedEndianess(int endian)
{
  // Endianess

  qDebug() << "Endianess Changed. Value = " << endian;

  // clear table on endianess change
  addItems();
}

void
MainWindow::openLogFile()
{
  // Open log file
  qDebug() << "Open log file";

  const auto arg{ "file:///" + QCoreApplication::applicationDirPath() +
                  "/QModMaster.log" };
  QDesktopServices::openUrl(QUrl{ arg });
}

void
MainWindow::openModbusManual()
{
  // Open Modbus Manual
  qDebug() << "Open Modbus Manual";

  const auto arg{ "file:///" + QCoreApplication::applicationDirPath() +
                  "/ManModbus/index.html" };
  QDesktopServices::openUrl(QUrl{ arg });
}

void
MainWindow::changedStartAddrBase(int curr_index)
{
  // Change Base

  qDebug() << "Start Addr Base changed. Index = " << curr_index;

  switch (curr_index) {
    case 0:
      m_ui->sbStartAddress->setDisplayIntegerBase(10);
      m_modbus->regModel->setStartAddrBase(10);
      break;
    case 1:
      m_ui->sbStartAddress->setDisplayIntegerBase(16);
      m_modbus->regModel->setStartAddrBase(16);
      break;
    default:
      m_ui->sbStartAddress->setDisplayIntegerBase(10);
      m_modbus->regModel->setStartAddrBase(10);
      break;
  }
}

void
MainWindow::changedStartAddress(int value)
{
  // Start Address changed
  qDebug() << "Start Address changed. Value = " << value;
  m_modbusCommSettings->setStartAddr(value);
  m_modbusCommSettings->saveSettings();

  m_modbus->setStartAddr(value);
  addItems();
}

void
MainWindow::changedNoOfRegs(int value)
{
  // No of regs changed
  qDebug() << "No Of Regs changed. Value = " << value;
  m_modbusCommSettings->setNoOfRegs(value);
  m_modbusCommSettings->saveSettings();

  m_modbus->setNumOfRegs(value);
  addItems();
}

void
MainWindow::changedFloatPrecision(int precision)
{
  // Float precision changed
  m_modbusCommSettings->setfloatPrecision(precision);
  m_modbusCommSettings->saveSettings();
  m_modbus->regModel->setFloatPrecision(precision);
}
void
MainWindow::updateStatusBar()
{
  // Update status bar

  QString msg;

  if (m_ui->cmbModbusMode->currentIndex() == 0) { // RTU
    msg = "RTU : ";
    msg += m_modbusCommSettings->serialPortName() + " | ";
    msg += m_modbusCommSettings->baud() + ",";
    msg += m_modbusCommSettings->dataBits() + ",";
    msg += m_modbusCommSettings->stopBits() + ",";
    msg += m_modbusCommSettings->parity();
  } else {
    msg = "TCP : ";
    msg += m_modbusCommSettings->slaveIP() + ":";
    msg += m_modbusCommSettings->tcpPort();
  }

  m_statusText->clear();
  m_statusText->setText(msg);

  // Connection is valid
  if (m_modbus->isConnected()) {
    m_statusInd->setPixmap(QPixmap(":/icons/bullet-green-16.png"));
  } else {
    m_statusInd->setPixmap(QPixmap(":/icons/bullet-red-16.png"));
  }

  // base Address
  m_baseAddr->setText("Base Addr : " + m_modbusCommSettings->baseAddr());

  // endianness
  m_endian->setText(("Endian : ") +
                    EUtils::endianness(m_modbusCommSettings->endian()));
}

void
MainWindow::addItems()
{
  // add items

  m_modbus->setSlave(m_ui->sbSlaveID->value());
  m_modbus->setFunctionCode(
    EUtils::modbusFunctionCode(m_ui->cmbFunctionCode->currentIndex()));
  m_modbus->setNumOfRegs(m_ui->sbNoOfRegs->value());
  // get base address
  const auto base_addr{ m_modbusCommSettings->baseAddr().toInt() };
  m_modbus->setStartAddr(m_ui->sbStartAddress->value() - base_addr);

  qInfo() << "Add Items. Function Code = "
              << QString::number(EUtils::modbusFunctionCode(
                                   m_ui->cmbFunctionCode->currentIndex()),
                                 16);

  m_modbus->addItems();
}

void
MainWindow::clearItems()
{
  // Clear items from registers model

  qDebug() << "clearItems";

  m_modbus->regModel->clear();
  addItems();
}

void
MainWindow::modbusRequest()
{
  // Request items from modbus adapter and add raw data to raw data model
  const auto row_count{ m_modbus->regModel->model->rowCount() };

  qDebug() << "Request transaction. No or registers = " << row_count;

  if (row_count == 0) {
    main_win->showUpInfoBar(tr("Request failed\nAdd items to Registers Table."),
                            InfoBar::Error);
    qWarning() << "Request failed. No items in registers table ";
    return;
  }
  if (m_modbus->regModel->getFrmt() == EUtils::Float &&
      m_ui->sbNoOfRegs->value() % 2 != 0) {
    main_win->showUpInfoBar(tr("The number of registers must be even."),
                            InfoBar::Error);
    qWarning() << "Request failed. The number of registers must be even ";
    return;
  } else {
    main_win->hideInfoBar();
  }

  // get base address
  const auto base_addr{ m_modbusCommSettings->baseAddr().toInt() };

  m_modbus->setSlave(m_ui->sbSlaveID->value());
  m_modbus->setFunctionCode(
    EUtils::modbusFunctionCode(m_ui->cmbFunctionCode->currentIndex()));
  m_modbus->setStartAddr(m_ui->sbStartAddress->value() - base_addr);
  m_modbus->setNumOfRegs(m_ui->sbNoOfRegs->value());

  // Modbus data
  m_modbus->modbusTransaction();
}

void
MainWindow::modbusScanCycle(bool value)
{
  // Request items from modbus adapter and add raw data to raw data model
  const auto row_count{ m_modbus->regModel->model->rowCount() };

  if (value && row_count == 0) {
    // mainWin->showUpInfoBar(tr("Request failed\nAdd items to Registers
    // Table."), InfoBar::Error);
    qWarning() << "Request failed. No items in registers table ";
    QMessageBox::critical(
      this, "QModMaster", tr("Request failed\nAdd items to Registers Table."));
    m_ui->actionScan->setChecked(false);
    return;
  }
  if (value && m_modbus->regModel->getFrmt() == EUtils::Float &&
      m_ui->sbNoOfRegs->value() % 2 != 0) {
    // mainWin->showUpInfoBar(tr("The number of registers must be even."),
    // InfoBar::Error);
    qWarning() << "Request failed. The number of registers must be even ";
    QMessageBox::critical(
      this, "QModMaster", tr("The number of registers must be even."));
    m_ui->actionScan->setChecked(false);
    return;
  } else {
    main_win->hideInfoBar();
  }

  // get base address
  const auto base_addr{ m_modbusCommSettings->baseAddr().toInt() };

  m_modbus->setSlave(m_ui->sbSlaveID->value());
  m_modbus->setFunctionCode(
    EUtils::modbusFunctionCode(m_ui->cmbFunctionCode->currentIndex()));
  m_modbus->setStartAddr(m_ui->sbStartAddress->value() - base_addr);
  m_modbus->setNumOfRegs(m_ui->sbNoOfRegs->value());

  // Start-Stop poll timer
  qDebug() << "Scan time = " << value;
  if (value) {
    if (m_ui->spInterval->value() <
        m_modbusCommSettings->timeOut().toInt() * 1000 * 2) {
      main_win->showUpInfoBar(tr("Scan rate  should be at least 2 * Timeout."),
                              InfoBar::Error);
      qWarning() << "Scan rate error. should be at least 2 * Timeout ";
    } else {
      m_modbus->setScanRate(m_ui->spInterval->value());
      m_modbus->startPollTimer();
    }
  } else {
    m_modbus->stopPollTimer();
  }

  // Update UI
  m_ui->cmbFunctionCode->setEnabled(!value);
  m_ui->sbSlaveID->setEnabled(!value);
  m_ui->sbStartAddress->setEnabled(!value);
  m_ui->spInterval->setEnabled(!value);
  m_ui->cmbStartAddrBase->setEnabled(!value);
  if (!value) {
    changedFunctionCode(m_ui->cmbFunctionCode->currentIndex());
  } else {
    m_ui->sbNoOfRegs->setEnabled(false);
  }
}

void
MainWindow::modbusConnect(bool connect)
{
  // Modbus connect - RTU/TCP
  qDebug() << "Modbus Connect. Value = " << connect;

  if (connect) { // RTU
    if (m_ui->cmbModbusMode->currentIndex() == EUtils::RTU) {
      m_modbus->setSlave(m_ui->sbSlaveID->value());
      m_modbus->modbusConnectRTU(m_modbusCommSettings->serialPortName(),
                                 m_modbusCommSettings->baud().toInt(),
                                 EUtils::parity(m_modbusCommSettings->parity()),
                                 m_modbusCommSettings->dataBits().toInt(),
                                 m_modbusCommSettings->stopBits().toInt(),
                                 m_modbusCommSettings->rts().toInt(),
                                 m_modbusCommSettings->timeOut().toInt());
    } else { // TCP
      m_modbus->modbusConnectTCP(m_modbusCommSettings->slaveIP(),
                                 m_modbusCommSettings->tcpPort().toInt(),
                                 m_modbusCommSettings->timeOut().toInt());
    }
  } else { // Disconnect
    m_modbus->modbusDisConnect();
    m_ui->actionScan->setChecked(false);
  }

  updateStatusBar();

  // Update UI
  m_ui->actionLoad_Session->setEnabled(!m_modbus->isConnected());
  m_ui->actionSave_Session->setEnabled(!m_modbus->isConnected());
  m_ui->actionConnect->setChecked(m_modbus->isConnected());
  m_ui->actionRead_Write->setEnabled(m_modbus->isConnected());
  m_ui->actionScan->setEnabled(m_modbus->isConnected());
  m_ui->cmbModbusMode->setEnabled(!m_modbus->isConnected());
}

void
MainWindow::showHeaders(bool value)
{
  qDebug() << "Show Headers = " << value;

  if (value) {
    m_ui->tblRegisters->horizontalHeader()->show();
    m_ui->tblRegisters->verticalHeader()->show();
  } else {
    m_ui->tblRegisters->horizontalHeader()->hide();
    m_ui->tblRegisters->verticalHeader()->hide();
  }
}

void
MainWindow::refreshView()
{
  qDebug() << "Packets sent / received = " << m_modbus->packets()
               << ", errors = " << m_modbus->errors();
  m_ui->tblRegisters->resizeColumnsToContents();

  m_statusPackets->setText(tr("Packets : ") +
                           QString("%1").arg(m_modbus->packets()));
  m_statusErrors->setText(tr("Errors : ") +
                          QString("%1").arg(m_modbus->errors()));
}

void
MainWindow::loadSession()
{
  qDebug() << "load session";
  const auto f_name{ QFileDialog::getOpenFileName(
    this,
    QStringLiteral("Load Session file"),
    QLatin1String(),
    QStringLiteral("Session Files (*.ses);;All Files (*.*)")) };

  // check
  if (f_name != "") {
    m_modbusCommSettings->loadSession(f_name);
    // Update UI
    m_ui->sbStartAddress->setMinimum(m_modbusCommSettings->baseAddr().toInt());
    m_ui->cmbFrmt->setCurrentIndex(m_modbusCommSettings->frmt());
    m_ui->sbPrecision->setValue(m_modbusCommSettings->floatPrecision());
    m_ui->cmbFunctionCode->setCurrentIndex(
      m_modbusCommSettings->functionCode());
    m_ui->cmbModbusMode->setCurrentIndex(m_modbusCommSettings->modbusMode());
    m_ui->sbSlaveID->setValue(m_modbusCommSettings->slaveID());
    m_ui->spInterval->setValue(m_modbusCommSettings->scanRate());
    m_ui->sbStartAddress->setValue(m_modbusCommSettings->startAddr());
    m_ui->sbNoOfRegs->setValue(m_modbusCommSettings->noOfRegs());
    updateStatusBar();
    refreshView();
    QMessageBox::information(
      this, "QModMaster", "Load session file : " + f_name);
  } else {
    QMessageBox::information(
      this, "QModMaster", "Cancel operation Or no file selected");
  }
}

void
MainWindow::saveSession()
{
  qDebug() << "save session";
  const auto f_name{ QFileDialog::getSaveFileName(
    this, "Save Session file", "", "Session Files (*.ses)") };

  // check
  if (f_name != "") {
    m_modbusCommSettings->saveSession(f_name);
    QMessageBox::information(
      this,
      QStringLiteral("QModMaster"),
      QStringLiteral("Save session file : %1").arg(f_name));
  } else {
    QMessageBox::information(
      this,
      QStringLiteral("QModMaster"),
      QStringLiteral("Cancel operation Or no file selected"));
  }
}
void
MainWindow::showUpInfoBar(const QString &message, InfoBar::InfoType type)
{
  m_ui->infobar->show(message, type);
}

void
MainWindow::hideInfoBar()
{
  m_ui->infobar->hide();
}

void
MainWindow::changeEvent(QEvent* event)
{
  if (event->type() == QEvent::LanguageChange) {
    m_ui->retranslateUi(this);
  }

  QMainWindow::changeEvent(event);
}

void
MainWindow::changeLanguage()
{
  // Not Used

  //  extern QTranslator* translator;
  //  QCoreApplication::removeTranslator(translator);
  //  translator->load(":/translations/" + QCoreApplication::applicationName() +
  //                   sender()->objectName().right(6));
  //  QCoreApplication::installTranslator(translator);
}
