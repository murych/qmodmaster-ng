#ifndef TOOLS_H
#define TOOLS_H

#include "controllers/modbusadapter.h"
#include "settings/modbuscommsettings.h"
#include <QComboBox>
#include <QMainWindow>
#include <QProcess>
#include <QTcpSocket>
#include <gsl-lite/gsl-lite.hpp>

namespace Ui {
class Tools;
}

class Tools : public QMainWindow
{
  Q_OBJECT

public:
  explicit Tools(ModbusAdapter* adapter,
                 ModbusCommSettings* settings,
                 QWidget* parent = nullptr);
  ~Tools() override;

private:
  std::unique_ptr<Ui::Tools> m_ui{ nullptr };
  std::unique_ptr<QComboBox> m_cmbModbusMode{ nullptr };
  std::unique_ptr<QComboBox> m_cmbCmd{ nullptr };
  ModbusAdapter* m_modbusAdapter{ nullptr };
  ModbusCommSettings* m_modbusCommSettings{ nullptr };
  QProcess m_pingProc;
  QTcpSocket m_portProc;

  [[nodiscard]] static auto ipConv(const QString& ip) -> QString;
  void pingProc();
  void portProc();
  void diagnosticsProc();
  void modbusDiagnostics();

  void exit();
  void changedModbusMode(int curr_index);
  void execCmd();
  void clear();
  void pingData();
};

#endif // TOOLS_H
