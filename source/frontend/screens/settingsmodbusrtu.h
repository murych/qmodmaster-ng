#ifndef SETTINGSMODBUSRTU_H
#define SETTINGSMODBUSRTU_H

#include "settings/modbuscommsettings.h"
#include <QDialog>
#include <QSettings>
#include <gsl-lite/gsl-lite.hpp>

namespace Ui {
class SettingsModbusRTU;
}

class SettingsModbusRTU : public QDialog
{
  Q_OBJECT

public:
  explicit SettingsModbusRTU(ModbusCommSettings* settings,
                             QWidget* parent = nullptr);
  ~SettingsModbusRTU() override;
  bool modbusConnected{ false };

private:
  std::unique_ptr<Ui::SettingsModbusRTU> m_ui{ nullptr };
  gsl::owner<ModbusCommSettings*> m_settings{ nullptr };

  void changesAccepted();

protected:
  void showEvent(QShowEvent* event) override;
};

#endif // SETTINGSMODBUSRTU_H
