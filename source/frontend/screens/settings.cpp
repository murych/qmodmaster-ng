#include "settings.h"
#include "ui_settings.h"

Settings::Settings(ModbusCommSettings* settings, QWidget* parent)
  : QDialog{ parent }
  , m_ui{ std::make_unique<Ui::Settings>() }
  , m_settings{ settings }
{
  qDebug() << this << "CONSTRUCTOR";
  Q_ASSERT(m_settings);

  m_ui->setupUi(this);

  connect(m_ui->buttonBox,
          &QDialogButtonBox::accepted,
          this,
          &Settings::changesAccepted);

  qDebug() << this << "CONSTRUCTED OK";
}

Settings::~Settings() = default;

void
Settings::showEvent(QShowEvent* event)
{
  qDebug() << this << "show event" << event;
  Q_ASSERT(m_settings);

  // Load Settings
  m_ui->sbMaxNoOfRawDataLines->setEnabled(!modbusConnected);
  m_ui->sbResponseTimeout->setEnabled(!modbusConnected);
  if (m_settings != nullptr) {
    m_ui->sbMaxNoOfRawDataLines->setValue(m_settings->maxNoOfLines().toInt());
    m_ui->sbResponseTimeout->setValue(m_settings->timeOut().toInt());
    m_ui->sbBaseAddr->setValue(m_settings->baseAddr().toInt());
    m_ui->cmbEndian->setCurrentIndex(m_settings->endian());
  }

  qDebug() << this << "ok";
}

void
Settings::changesAccepted()
{
  // Save Settings
  if (m_settings != nullptr) {
    m_settings->setMaxNoOfLines(m_ui->sbMaxNoOfRawDataLines->cleanText());
    m_settings->setTimeOut(m_ui->sbResponseTimeout->cleanText());
    m_settings->setBaseAddr(m_ui->sbBaseAddr->cleanText());

    if (m_settings->endian() != m_ui->cmbEndian->currentIndex()) {
      emit changedEndianess(m_ui->cmbEndian->currentIndex());
    }
    m_settings->setEndian(m_ui->cmbEndian->currentIndex());
  }
}
