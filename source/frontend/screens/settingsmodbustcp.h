#ifndef SETTINGSMODBUSTCP_H
#define SETTINGSMODBUSTCP_H

#include "settings/modbuscommsettings.h"
#include <QAbstractButton>
#include <QDialog>
#include <QSettings>
#include <gsl-lite/gsl-lite.hpp>

namespace Ui {
class SettingsModbusTCP;
}

class SettingsModbusTCP : public QDialog
{
  Q_OBJECT

public:
  explicit SettingsModbusTCP(ModbusCommSettings* settings,
                             QWidget* parent = nullptr);
  ~SettingsModbusTCP() override;

  bool modbusConnected{ false };

private:
  std::unique_ptr<Ui::SettingsModbusTCP> m_ui{ nullptr };
  gsl::owner<ModbusCommSettings*> m_settings{ nullptr };

  int validateInputs();

  void changesAccepted();

protected:
  void showEvent(QShowEvent* event) override;
};

#endif // SETTINGSMODBUSTCP_H
