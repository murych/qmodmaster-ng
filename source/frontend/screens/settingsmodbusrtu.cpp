#include "settingsmodbusrtu.h"
#include "ui_settingsmodbusrtu.h"

SettingsModbusRTU::SettingsModbusRTU(ModbusCommSettings* settings,
                                     QWidget* parent)
  : QDialog{ parent }
  , m_ui{ std::make_unique<Ui::SettingsModbusRTU>() }
  , m_settings{ settings }
{
  qDebug() << this << "CONSTRUCTOR";

  m_ui->setupUi(this);

/* device name is needed only in Linux */
#ifdef Q_OS_WIN32
  m_ui->cmbDev->setDisabled(true);
#else
  m_ui->cmbDev->setDisabled(false);
#endif

  connect(m_ui->buttonBox,
          &QDialogButtonBox::accepted,
          this,
          &SettingsModbusRTU::changesAccepted);
}

SettingsModbusRTU::~SettingsModbusRTU() = default;

void
SettingsModbusRTU::showEvent(QShowEvent* event)
{
  // Load Settings
  m_ui->cmbDataBits->setEnabled(!modbusConnected);
  m_ui->cmbBaud->setEnabled(!modbusConnected);
  m_ui->sbPort->setEnabled(!modbusConnected);
  m_ui->cmbParity->setEnabled(!modbusConnected);
  m_ui->cmbRTS->setEnabled(!modbusConnected);
  m_ui->cmbStopBits->setEnabled(!modbusConnected);
  if (m_settings != nullptr) {

    m_ui->cmbRTS->clear();

// Populate cmbPort-cmbRTS
#ifdef Q_OS_WIN32
    m_ui->cmbRTS->addItem("Disable");
    m_ui->cmbRTS->addItem("Enable");
    m_ui->cmbRTS->addItem("HandShake");
    m_ui->cmbRTS->addItem("Toggle");
#else
    m_ui->cmbRTS->addItem("None");
    m_ui->cmbRTS->addItem("Up");
    m_ui->cmbRTS->addItem("Down");
#endif

    m_ui->cmbDev->setCurrentText(m_settings->serialDev());
    m_ui->sbPort->setValue(m_settings->serialPort().toInt());
    m_ui->cmbBaud->setCurrentIndex(m_ui->cmbBaud->findText(m_settings->baud()));
    m_ui->cmbDataBits->setCurrentIndex(
      m_ui->cmbDataBits->findText(m_settings->dataBits()));
    m_ui->cmbStopBits->setCurrentIndex(
      m_ui->cmbStopBits->findText(m_settings->stopBits()));
    m_ui->cmbParity->setCurrentIndex(
      m_ui->cmbParity->findText(m_settings->parity()));
    m_ui->cmbRTS->setCurrentIndex(m_ui->cmbRTS->findText(m_settings->rts()));
  }
}

void
SettingsModbusRTU::changesAccepted()
{
  // Save Settings
  if (m_settings != nullptr) {

    m_settings->setSerialPort(QString::number(m_ui->sbPort->value()),
                              m_ui->cmbDev->currentText());
    m_settings->setBaud(m_ui->cmbBaud->currentText());
    m_settings->setDataBits(m_ui->cmbDataBits->currentText());
    m_settings->setStopBits(m_ui->cmbStopBits->currentText());
    m_settings->setParity(m_ui->cmbParity->currentText());
    m_settings->setRTS((QString)m_ui->cmbRTS->currentText());
  }
}
