#include "busmonitor.h"
#include "ui_busmonitor.h"

#include <QCloseEvent>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QShowEvent>

BusMonitor::BusMonitor(RawDataModel* raw_data_model, QWidget* parent)
  : QMainWindow{ parent }
  , m_ui{ std::make_unique<Ui::BusMonitor>() }
  , m_rawDataModel{ raw_data_model }
{
  qDebug() << this << "Constructor";
  Q_ASSERT(m_rawDataModel);
  Q_ASSERT(m_rawDataModel->model);

  m_ui->setupUi(this);
  m_ui->lstRawData->setModel(m_rawDataModel->model.get());

  // Setup Toolbar
  m_ui->toolBar->addAction(m_ui->actionSave);
  m_ui->toolBar->addAction(m_ui->actionClear);
  m_ui->toolBar->addAction(m_ui->actionSxS);
  m_ui->toolBar->addAction(m_ui->actionExit);

  connect(m_ui->actionSave, &QAction::triggered, this, &BusMonitor::save);
  connect(m_ui->actionClear, &QAction::triggered, this, &BusMonitor::clear);
  connect(m_ui->actionSxS, &QAction::triggered, this, &BusMonitor::SxS);
  connect(m_ui->actionExit, &QAction::triggered, this, &BusMonitor::exit);

  connect(
    m_ui->lstRawData, &QListView::activated, this, &BusMonitor::selectedRow);
  connect(
    m_ui->lstRawData, &QListView::clicked, this, &BusMonitor::selectedRow);

  m_ui->lblADU2->setVisible(false);
  m_ui->txtADU2->setVisible(false);
}

BusMonitor::~BusMonitor() = default;

void
BusMonitor::save()
{
  // Select file
  const auto file_name{ QFileDialog::getSaveFileName(
    this,
    QStringLiteral("Save File As..."),
    QDir::homePath(),
    QStringLiteral("Text (*.txt)"),
    nullptr,
    QFileDialog::DontConfirmOverwrite) };

  // Open File
  if (file_name.isEmpty()) {
    return;
  }

  // continue only if a file name exists
  QFile file{ file_name };
  if (!file.open(QIODevice::WriteOnly)) {
    return;
  }

  // Text Stream
  QTextStream ts{ &file };
  auto sl{ m_rawDataModel->model->stringList() };

  // iterate
  for (const auto& i : sl) {
    ts << i << Qt::endl;
  }

  // Close File
  file.close();
}

void
BusMonitor::clear()
{
  m_rawDataModel->clear();
  m_ui->txtADU1->clear();
}

void
BusMonitor::SxS()
{
  if (m_ui->actionSxS->isChecked()) {
    m_ui->lblADU1->setText(QStringLiteral("Tx ADU"));
    m_ui->lblADU2->setVisible(true);
    m_ui->txtADU2->setVisible(true);
  } else {
    m_ui->lblADU1->setText(QStringLiteral("ADU"));
    m_ui->lblADU2->setVisible(false);
    m_ui->txtADU2->setVisible(false);
  }

  m_ui->txtADU1->setPlainText(QLatin1String());
  m_ui->txtADU2->setPlainText(QLatin1String());
}

void
BusMonitor::exit()
{
  close();
}

void
BusMonitor::closeEvent(QCloseEvent* event)
{
  m_rawDataModel->enableAddLines(false);
  clear();
  event->accept();
}

void
BusMonitor::showEvent(QShowEvent* event)
{
  m_rawDataModel->enableAddLines(true);
  event->accept();
}

void
BusMonitor::selectedRow(const QModelIndex& selected)
{
  int row_count = 0;
  int curr_row  = 0;
  QModelIndex next;
  QModelIndex prev;

  row_count = m_ui->lstRawData->model()->rowCount();
  curr_row  = selected.row();
  if (curr_row < row_count - 1) {
    next = m_ui->lstRawData->model()->index(curr_row + 1, 0);
    qDebug() << "Next Row : " << next.row();
    qDebug() << "Next Data : " << next.data();
  }

  if (m_ui->actionSxS->isChecked()) { // Side by Side Tx - Rx packests
    if (selected.data().canConvert(QVariant::String)) {
      auto val{ selected.data().value<QString>() };
      qDebug() << "BusMonitor : selectedRow - " << val;
      if (val.indexOf("Sys") > -1) {
        parseSysMsg(val, m_ui->txtADU1);
      } else if (val.indexOf("Tx") > -1) {
        parseTxMsg(val, m_ui->txtADU1);
        if (curr_row < row_count - 1) {
          next = m_ui->lstRawData->model()->index(curr_row + 1, 0);
          auto val_next{ next.data().value<QString>() };
          if (val_next.indexOf("Rx") > -1) {
            parseRxMsg(val_next, m_ui->txtADU2);
          }
        } else {
          m_ui->txtADU2->setPlainText(QStringLiteral("-"));
        }
      } else if (val.indexOf("Rx") > -1) {
        parseRxMsg(val, m_ui->txtADU2);
        if (curr_row > 0) {
          prev = m_ui->lstRawData->model()->index(curr_row - 1, 0);
          auto val_prev{ prev.data().value<QString>() };
          if (val_prev.indexOf("Tx") > -1) {
            parseTxMsg(val_prev, m_ui->txtADU1);
          }
        } else {
          m_ui->txtADU1->setPlainText(QStringLiteral("-"));
        }
      } else {
        m_ui->txtADU1->setPlainText(QStringLiteral("Type : Unknown Message"));
      }
    }
  } else {
    if (selected.data().canConvert(QVariant::String)) {
      auto val = selected.data().value<QString>();
      qDebug() << "BusMonitor : selectedRow - " << val;
      if (val.indexOf("Sys") > -1) {
        parseSysMsg(val, m_ui->txtADU1);
      } else if (val.indexOf("Tx") > -1) {
        parseTxMsg(val, m_ui->txtADU1);
      } else if (val.indexOf("Rx") > -1) {
        parseRxMsg(val, m_ui->txtADU1);
      } else {
        m_ui->txtADU1->setPlainText(QStringLiteral("Type : Unknown Message"));
      }
    }
  }
}

void
BusMonitor::parseTxMsg(const QString& msg, QPlainTextEdit* txt_adu)
{
  txt_adu->setPlainText(QStringLiteral("Type : Tx Message"));
  auto row{ msg.split(QRegularExpression{ "\\s+" }) };
  txt_adu->appendPlainText(QStringLiteral("Timestamp : %1").arg(row.at(2)));
  if (msg.indexOf("RTU") > -1) { // RTU message
    QStringList pdu;
    if (row.length() < 5) { // check message length
      txt_adu->appendPlainText(QStringLiteral("Error! Cannot parse Message"));
      return;
    }
    for (int i = 4; i < row.length() - 1; ++i) {
      pdu.append(row.at(i));
    }
    parseTxPDU(pdu, QStringLiteral("Slave Addr : "), txt_adu);
    txt_adu->appendPlainText(
      QStringLiteral("CRC : %1 %2")
        .arg(pdu.at(pdu.length() - 2), pdu.at(pdu.length() - 1)));
  } else if (msg.indexOf("TCP") > -1) { // TCP message
    if (row.length() < 11) {            // check message length
      txt_adu->appendPlainText(QStringLiteral("Error! Cannot parse Message"));
      return;
    }
    txt_adu->appendPlainText("Transaction ID : " + row[4] + row[5]);
    txt_adu->appendPlainText("Protocol ID : " + row[6] + row[7]);
    txt_adu->appendPlainText("Length : " + row[8] + row[9]);
    QStringList pdu;
    for (int i = 10; i < row.length() - 1; i++) {
      pdu.append(row[i]);
    }
    parseTxPDU(pdu, "Unit ID : ", txt_adu);
  } else {
    txt_adu->appendPlainText(QStringLiteral("Error! Cannot parse Message"));
  }
}

void
BusMonitor::parseTxPDU(const QStringList& pdu,
                       const QString& slave,
                       QPlainTextEdit* txt_adu)
{

  if (pdu.length() < 6) { // check message length
    txt_adu->appendPlainText(slave + pdu[0]);
    txt_adu->appendPlainText("Function Code : " + pdu[1]);
    // txtADU->appendPlainText("Error! Cannot parse Message");
    return;
  }
  txt_adu->appendPlainText(slave + pdu[0]);
  txt_adu->appendPlainText("Function Code : " + pdu[1]);
  txt_adu->appendPlainText("Starting Address : " + pdu[2] + pdu[3]);
  bool ok   = false;
  int fcode = pdu[1].toInt(&ok, 16);
  if (fcode == 1 || fcode == 2 || fcode == 3 || fcode == 4) { // read
    txt_adu->appendPlainText("Quantity of Registers : " + pdu[4] + pdu[5]);
  } else if (fcode == 5 || fcode == 6) { // write
    txt_adu->appendPlainText("Output Value : " + pdu[4] + pdu[5]);
  } else if (fcode == 15 || fcode == 16) { // write multiple
    txt_adu->appendPlainText("Quantity of Registers : " + pdu[4] + pdu[5]);
    if (pdu.length() < 8) { // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    txt_adu->appendPlainText("Byte Count : " + pdu[6]);
    int byte_count        = pdu[6].toInt(&ok, 16);
    QString output_values = "";
    for (int i = 7; i < 7 + byte_count; i++) {
      output_values += pdu[i] + " ";
    }
    txt_adu->appendPlainText("Output Values : " + output_values);
  }
}

void
BusMonitor::parseRxMsg(const QString& msg, QPlainTextEdit* txt_adu)
{
  txt_adu->setPlainText("Type : Rx Message");
  QStringList row = msg.split(QRegularExpression("\\s+"));
  txt_adu->appendPlainText("Timestamp : " + row[2]);
  if (msg.indexOf("RTU") > -1) { // RTU message
    QStringList pdu;
    if (row.length() < 5) { // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    for (int i = 4; i < row.length() - 1; i++) {
      pdu.append(row[i]);
    }
    parseRxPDU(pdu, "Slave Addr : ", txt_adu);
    txt_adu->appendPlainText("CRC : " + pdu[pdu.length() - 2] +
                             pdu[pdu.length() - 1]);
  } else if (msg.indexOf("TCP") > -1) { // TCP message
    if (row.length() < 11) {            // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    txt_adu->appendPlainText("Transaction ID : " + row[4] + row[5]);
    txt_adu->appendPlainText("Protocol ID : " + row[6] + row[7]);
    txt_adu->appendPlainText("Length : " + row[8] + row[9]);
    QStringList pdu;
    for (int i = 10; i < row.length() - 1; i++) {
      pdu.append(row[i]);
    }
    parseRxPDU(pdu, "Unit ID : ", txt_adu);
  } else {
    txt_adu->appendPlainText("Error! Cannot parse Message");
  }
}

void
BusMonitor::parseRxPDU(const QStringList& pdu,
                       const QString& slave,
                       QPlainTextEdit* txt_adu)
{

  bool ok   = false;
  int fcode = pdu[1].toInt(&ok, 16);
  if (fcode == 1 || fcode == 2 || fcode == 3 || fcode == 4) { // read
    if (pdu.length() < 4) { // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    txt_adu->appendPlainText(slave + pdu[0]);
    txt_adu->appendPlainText("Function Code : " + pdu[1]);
    txt_adu->appendPlainText("Byte Count : " + pdu[2]);
    int byte_count       = pdu[2].toInt(&ok, 16);
    QString input_values = "";
    for (int i = 3; i < 3 + byte_count; i++) {
      input_values += pdu[i] + " ";
    }
    txt_adu->appendPlainText("Register Values : " + input_values);
  } else if (fcode == 5 || fcode == 6) { // write
    if (pdu.length() < 6) {              // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    txt_adu->appendPlainText(slave + pdu[0]);
    txt_adu->appendPlainText("Function Code : " + pdu[1]);
    txt_adu->appendPlainText("Starting Address : " + pdu[2] + pdu[3]);
    txt_adu->appendPlainText("Output Value : " + pdu[4] + pdu[5]);
  } else if (fcode == 15 || fcode == 16) { // write multiple
    if (pdu.length() < 6) {                // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    txt_adu->appendPlainText(slave + pdu[0]);
    txt_adu->appendPlainText("Function Code : " + pdu[1]);
    txt_adu->appendPlainText("Starting Address : " + pdu[2] + pdu[3]);
    txt_adu->appendPlainText("Quantity of Registers : " + pdu[4] + pdu[5]);
  } else if (fcode > 0x80) { // exception
    if (pdu.length() < 3) {  // check message length
      txt_adu->appendPlainText("Error! Cannot parse Message");
      return;
    }
    txt_adu->appendPlainText(slave + pdu[0]);
    txt_adu->appendPlainText("Function Code [80 + Rx Function Code] : " +
                             pdu[1]);
    txt_adu->appendPlainText("Exception Code : " + pdu[2]);
  }
}

void
BusMonitor::parseSysMsg(const QString& msg, QPlainTextEdit* txt_adu)
{
  txt_adu->setPlainText("Type : System Message");
  QStringList row = msg.split(QRegularExpression("\\s+"));
  txt_adu->appendPlainText("Timestamp : " + row[2]);
  txt_adu->appendPlainText("Message" + msg.mid(msg.indexOf(" : ")));
}
