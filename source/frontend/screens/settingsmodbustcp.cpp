#include "settingsmodbustcp.h"
#include "ui_settingsmodbustcp.h"

#include <QMessageBox>

SettingsModbusTCP::SettingsModbusTCP(ModbusCommSettings* settings,
                                     QWidget* parent)
  : QDialog{ parent }
  , m_ui{ std::make_unique<Ui::SettingsModbusTCP>() }
  , m_settings{ settings }
{
  qDebug() << this << "CONSTRUCTOR";

  m_ui->setupUi(this);

  connect(m_ui->buttonBox,
          &QDialogButtonBox::accepted,
          this,
          &SettingsModbusTCP::changesAccepted);
}

SettingsModbusTCP::~SettingsModbusTCP() = default;

void
SettingsModbusTCP::showEvent(QShowEvent* event)
{
  // Load Settings
  m_ui->leSlaveIP->setEnabled(!modbusConnected);
  m_ui->leTCPPort->setEnabled(!modbusConnected);
  if (m_settings != nullptr) {
    m_ui->leTCPPort->setText(m_settings->tcpPort());
    m_ui->leSlaveIP->setText(m_settings->slaveIP());
  }
}

void
SettingsModbusTCP::changesAccepted()
{
  const auto validation{ validateInputs() };

  switch (validation) {
    case 0: // ok
      // Save Settings
      if (m_settings != nullptr) {
        m_settings->setTCPPort(m_ui->leTCPPort->text());
        m_settings->setSlaveIP(m_ui->leSlaveIP->text());
      }
      break;
    case 1: // wrong ip
      QMessageBox::critical(this,
                            QStringLiteral("Modbus TCP Settings"),
                            QStringLiteral("Wrong IP Address."));
      break;
    case 2: // wrong port
      QMessageBox::critical(this,
                            QStringLiteral("Modbus TCP Settings"),
                            QStringLiteral("Wrong Port Number."));
      break;
  }
}

int
SettingsModbusTCP::validateInputs()
{
  // Strip zero's from IP
  bool ok{ false };

  const auto ipBytes{ (m_ui->leSlaveIP->text()).split(".") };
  if (ipBytes.size() == 4) {
    for (const auto& i : ipBytes) {
      const auto ip_byte{ i.toInt(&ok) };
      if (!ok || ip_byte > 255) {
        return 1; // wrong ip
      }
    }
  } else {
    return 1; // wrong ip
  }

  const auto port{ (m_ui->leTCPPort->text()).toInt(&ok) };
  if (!ok || port <= 0 || port > 65535) {
    return 2; // wrong port
  }

  return 0; // validate ok
}
