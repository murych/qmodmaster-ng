#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "controllers/modbusadapter.h"
#include "screens/about.h"
#include "screens/busmonitor.h"
#include "screens/settings.h"
#include "screens/settingsmodbusrtu.h"
#include "screens/settingsmodbustcp.h"
#include "screens/tools.h"
#include "settings/modbuscommsettings.h"
#include "widgets/infobar.h"
#include <QLabel>
#include <QMainWindow>
#include <QProcess>
#include <QSettings>
#include <QString>
#include <gsl-lite/gsl-lite.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(ModbusAdapter* adapter,
                      ModbusCommSettings* settings,
                      QWidget* parent = nullptr);
  ~MainWindow() override;

  void showUpInfoBar(const QString& message, InfoBar::InfoType type);
  void hideInfoBar();

private:
  std::unique_ptr<Ui::MainWindow> m_ui{ nullptr };
  // UI - Dialogs
  std::unique_ptr<About> m_dlgAbout{ nullptr };
  std::unique_ptr<SettingsModbusRTU> m_dlgModbusRTU{ nullptr };
  std::unique_ptr<SettingsModbusTCP> m_dlgModbusTCP{ nullptr };
  std::unique_ptr<Settings> m_dlgSettings{ nullptr };
  std::unique_ptr<BusMonitor> m_busMonitor{ nullptr };
  std::unique_ptr<Tools> m_tools{ nullptr };

  ModbusCommSettings* m_modbusCommSettings{ nullptr };
  void updateStatusBar();
  QLabel* m_statusText;
  QLabel* m_statusInd;
  QLabel* m_baseAddr;
  QLabel* m_statusPackets;
  QLabel* m_statusErrors;
  QLabel* m_endian;
  ModbusAdapter* m_modbus{ nullptr };
  void modbusConnect(bool connect);

  void changeEvent(QEvent* event) override;

  void showSettingsModbusRTU();
  void showSettingsModbusTCP();
  void showSettings();
  void showBusMonitor();
  void showTools();
  void changedModbusMode(int curr_index);
  void changedFunctionCode(int curr_index);
  void changedFrmt(int curr_index);
  void changedDecSign(bool value);
  void changedStartAddrBase(int curr_index);
  void changedScanRate(int value);
  void changedConnect(bool value);
  void changedStartAddress(int value);
  void changedNoOfRegs(int value);
  void changedSlaveID(int value);
  void changedFloatPrecision(int precision);
  void changedEndianess(int endian);
  void addItems();
  void clearItems();
  static void openLogFile();
  void modbusScanCycle(bool value);
  void modbusRequest();

  void changeLanguage();
  static void openModbusManual();
  void loadSession();
  void saveSession();
  void showHeaders(bool value);

signals:
  void resetCounters();

public slots:
  void refreshView();
};

extern gsl::owner<MainWindow*> main_win;

#endif // MAINWINDOW_H
