#include "registersdatadelegate.h"

#include "screens/mainwindow.h"
#include "utils/eutils.h"
#include <QLineEdit>
#include <QPainter>
#include <QSpinBox>

void
RegistersDataDelegate::paint(QPainter* painter,
                             const QStyleOptionViewItem& option,
                             const QModelIndex& index) const
{
  QStyledItemDelegate::paint(painter, option, index);
}

QWidget*
RegistersDataDelegate::createEditor(QWidget* parent,
                                    const QStyleOptionViewItem& /* option */,
                                    const QModelIndex& /* index */) const
{
  if (m_frmt == EUtils::Bin) { // Bin
    if (m_is16Bit) {
      auto* editor{ new QLineEdit{ parent } };
      editor->setInputMask("bbbbbbbbbbbbbbbb");
      return editor;
    }
    auto* editor{ new QSpinBox{ parent } };
    editor->setMinimum(0);
    editor->setMaximum(1);
    return editor;

  } else if (m_frmt == EUtils::Dec) { // Dec
    const QRegularExpression rx{ "-{0,1}[0-9]{1,5}" };
    auto* validator{ new QRegularExpressionValidator{ rx } };
    auto* editor{ new QLineEdit{ parent } };
    editor->setValidator(validator);
    return editor;
  } else if (m_frmt == EUtils::Float) { // Float
    const QRegularExpression rx{ "^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$" };
    auto* validator{ new QRegularExpressionValidator{ rx } };
    auto* editor{ new QLineEdit{ parent } };
    editor->setValidator(validator);
    return editor;
  } else if (m_frmt == EUtils::Hex) { // Hex
    auto* editor{ new QLineEdit{ parent } };
    editor->setInputMask("hhhh");
    return editor;
  } else { // Default = Dec
    const QRegularExpression rx{ "-{0,1}[0-9]{1,5}" };
    auto* validator{ new QRegularExpressionValidator{ rx } };
    auto* editor{ new QLineEdit{ parent } };
    editor->setValidator(validator);
    return editor;
  }
}

void
RegistersDataDelegate::setEditorData(QWidget* editor,
                                     const QModelIndex& index) const
{
  auto value{ index.model()->data(index, Qt::EditRole).toString() };

  if (m_frmt == EUtils::Bin && !m_is16Bit) { // Bin
    auto* spin_box = dynamic_cast<QSpinBox*>(editor);
    spin_box->setValue(value.toInt());
  } else { // Bin 16 Bit, Dec, Hex, Float?
    auto* line_edit = dynamic_cast<QLineEdit*>(editor);
    line_edit->setText(value);
  }
}

void
RegistersDataDelegate::setModelData(QWidget* editor,
                                    QAbstractItemModel* model,
                                    const QModelIndex& index) const
{
  QString value;
  int intVal{ 0 };
  float float_val{ 0 };
  bool ok{ false };

  if (m_frmt == EUtils::Bin && !m_is16Bit) { // Bin
    auto* spin_box{ dynamic_cast<QSpinBox*>(editor) };
    intVal = (spin_box->text()).toInt(&ok, m_frmt);
    value = EUtils::formatValue(intVal, m_frmt, m_is16Bit, m_isSigned);
  } else if (m_frmt == EUtils::Float) { // Float
    auto* line_edit{ dynamic_cast<QLineEdit*>(editor) };
    float_val = line_edit->text().toFloat(&ok);
    value     = EUtils::formatValue32(float_val, m_floatPrecision);
  } else { // Bin 16 Bit, Dec, Hex
    auto* line_edit{ dynamic_cast<QLineEdit*>(editor) };
    intVal = line_edit->text().toInt(&ok, m_frmt);
    if (intVal > 65535) {
      main_win->showUpInfoBar(
        tr("Set value failed\nValue is greater than 65535."), InfoBar::Error);
      qWarning() << "Set value failed. Value is greater than 65535";
      return;
    }
    if (intVal < -32768) {
      main_win->showUpInfoBar(
        tr("Set value failed\nValue is smaller than -32768."), InfoBar::Error);
      qWarning() << "Set value failed. Value is smaller than -32768";
      return;
    } else {
      main_win->hideInfoBar();
    }
    value = EUtils::formatValue(intVal, m_frmt, m_is16Bit, m_isSigned);
  }

  qDebug() << "Set model data value = " << value;

  model->setData(index, value, Qt::EditRole);
}

void
RegistersDataDelegate::updateEditorGeometry(
  QWidget* editor,
  const QStyleOptionViewItem& option,
  const QModelIndex& /* index */) const
{
  editor->setGeometry(option.rect);
}
