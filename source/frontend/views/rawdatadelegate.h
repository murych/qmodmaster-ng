#ifndef RAWDATADELEGATE_H
#define RAWDATADELEGATE_H

#include <QStyledItemDelegate>

class RawDataDelegate : public QStyledItemDelegate
{
  Q_OBJECT

public:
  using QStyledItemDelegate::QStyledItemDelegate;

  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
};
#endif // RAWDATADELEGATE_H
