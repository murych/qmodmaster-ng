#ifndef REGISTERSDELEGATE_H
#define REGISTERSDELEGATE_H

#include <QStyledItemDelegate>

class RegistersDataDelegate : public QStyledItemDelegate
{
  Q_OBJECT

private:
  int m_frmt{ 0 };
  bool m_is16Bit{ false };
  bool m_isSigned{ false };
  int m_floatPrecision{ 0 };

public:
  using QStyledItemDelegate::QStyledItemDelegate;

  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;

  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;

  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;

  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;

  void setFrmt(int frmt) { m_frmt = frmt; }
  void setIs16Bit(bool is16_bit) { m_is16Bit = is16_bit; }
  void setIsSigned(bool is_signed) { m_isSigned = is_signed; }
  void setFloatPrecision(int precision) { m_floatPrecision = precision; }
};

#endif // REGISTERSDELEGATE_H
