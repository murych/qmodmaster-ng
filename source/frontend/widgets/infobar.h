#ifndef _MyInfoBar_H_
#define _MyInfoBar_H_

#include <QFrame>
#include <QLabel>

class InfoBar : public QFrame
{
  Q_OBJECT
private:
  std::unique_ptr<QLabel> m_label{ nullptr };

public:
  enum InfoType
  {
    Question,
    Warning,
    Error
  };

  explicit InfoBar(QWidget* parent = nullptr);
  InfoBar(const QString& message,
          InfoType type,
          QWidget* panullptrent = nullptr);
  ~InfoBar() override;

  void setInfoType(InfoType type);
  void setMessage(const QString& message);
  void show(const QString& message, InfoType type);
};

#endif
