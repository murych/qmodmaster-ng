#include "infobar.h"

#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

InfoBar::InfoBar(QWidget* parent)
  : QFrame{ parent }
  , m_label{ std::make_unique<QLabel>() }
{
  auto* button{ new QPushButton{ this } };
  button->setStyleSheet("QPushButton{image: url(:/icons/close8-black.png);\
                                       background-color: lightgrey;\
                                       border: none;\
                                       height: 12px;\
                                       width: 12px}\
                           QPushButton:hover{image: url(:/icons/close8-red.png);\
                                             background-color: lightgrey;\
                                             border: none;\
                                             height: 12px;\
                                             width: 12px}\
                           QPushButton:pressed{image: url(:/icons/close8-red.png);\
                                             background-color: lightgrey;\
                                             border: 1px solid darkgrey;\
                                             border-radius: 2px;\
                                             height: 12px;\
                                             width: 12px}");
  button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  button->setFocusPolicy(Qt::NoFocus);
  connect(button, &QAbstractButton::clicked, this, &QWidget::hide);

  auto* hboxlayout = new QHBoxLayout{ this };
  hboxlayout->addWidget(m_label.get());
  hboxlayout->addWidget(button);

  setLayout(hboxlayout);
  setFrameStyle(QFrame::Box);
  hide();
}

InfoBar::InfoBar(const QString& message, InfoType type, QWidget* parent)
  : InfoBar{ parent }
{
  setMessage(message);
  setInfoType(type);
}

InfoBar::~InfoBar() = default;

void
InfoBar::setInfoType(InfoType type)
{
  switch (type) {
    case Question:
      setStyleSheet("color: black;\
                          background-color: skyblue");
      break;
    case Error:
      setStyleSheet("color: white;\
                          background-color: red");
      break;
    default:
    case Warning:
      setStyleSheet("color: black;\
                          background-color: yellow");
      break;
  }
}

void
InfoBar::setMessage(const QString& message)
{
  m_label->setText(message);
}

void
InfoBar::show(const QString& message, InfoType type)
{
  setMessage(message);
  setInfoType(type);
  QFrame::show();
}
