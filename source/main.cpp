#include "controllers/modbusadapter.h"
#include "screens/mainwindow.h"
#include "settings/modbuscommsettings.h"
#include <QApplication>
#include <QDir>
#include <QTranslator>

#include <cstdio>
#include <cstdlib>

QTranslator* translator;

// Logging Levels
// TraceLevel : 0
// DebugLevel : 1
// InfoLevel : 2
// WarnLevel : 3
// ErrorLevel : 4
// FatalLevel : 5
// OffLevel : 6

int
main(int argc, char* argv[])
{
  QApplication app{ argc, argv };

  translator = new QTranslator;
  translator->load(":/translations/" + QCoreApplication::applicationName() +
                   "_" + QLocale::system().name());
  QApplication::installTranslator(translator);




  // Modbus Adapter
  ModbusAdapter modbus_adapt;
  // Program settings
  ModbusCommSettings settings{ "qModMaster.ini" };

  // show main window
  main_win = new MainWindow{ &modbus_adapt, &settings };

  QObject::connect(&modbus_adapt,
                   &ModbusAdapter::refreshView,
                   main_win,
                   &MainWindow::refreshView);

  QObject::connect(main_win,
                   &MainWindow::resetCounters,
                   &modbus_adapt,
                   &ModbusAdapter::resetCounters);

  main_win->show();

  const auto ret{ QApplication::exec() };

  return ret;
}
