#ifndef MODBUSADAPTER_H
#define MODBUSADAPTER_H

#include "models/rawdatamodel.h"
#include "models/registersmodel.h"
#include "utils/eutils.h"
#include <QObject>
#include <QTimer>
#include <gsl-lite/gsl-lite.hpp>
#include <modbus.h>

class ModbusAdapter : public QObject
{
  Q_OBJECT
public:
  explicit ModbusAdapter(QObject* parent = nullptr);
  ~ModbusAdapter() override;

  void busMonitorRequestData(uint8_t* data, int data_len);
  void busMonitorResponseData(uint8_t* data, int data_len);

  void modbusConnectRTU(const QString& port,
                        int baud,
                        QChar parity,
                        int data_bits,
                        int stop_bits,
                        int rts,
                        int time_out = 1);
  void modbusConnectTCP(const QString& ip, int port, int time_out = 1);
  void modbusDisConnect();
  std::unique_ptr<RegistersModel> regModel{ nullptr };
  std::unique_ptr<RawDataModel> rawModel{ nullptr };
  [[nodiscard]] bool isConnected() const { return m_connected; }

  void setSlave(int slave) { m_slave = slave; }
  void setFunctionCode(int function_code) { m_functionCode = function_code; }
  void setStartAddr(int addr) { m_startAddr = addr; }
  void setNumOfRegs(int num) { m_numOfRegs = num; }
  void setBaseAddr(int base_addr) { m_baseAddr = base_addr; }
  void addItems();
  void setReadOutputsBeforeWrite(bool read_outputs_before_write);

  void setScanRate(int scan_rate);
  void setTimeOut(int time_out) { m_timeOut = time_out; }
  void startPollTimer();
  void stopPollTimer();
  [[nodiscard]] auto packets() const { return m_packets; }
  [[nodiscard]] auto errors() const { return m_errors; }
  gsl::owner<modbus_t*> m_modbus{ nullptr };

private:
  void modbusReadData(int slave,
                      int function_code,
                      int start_address,
                      int no_of_items);
  void modbusWriteData(int slave,
                       int function_code,
                       int start_address,
                       int no_of_items);
  static QString stripIP(const QString& ip);
  bool m_connected{ false };
  int m_modBusMode{ EUtils::None };
  int m_slave{ 0 };
  int m_functionCode{ 0 };
  int m_startAddr{ 0 };
  int m_baseAddr{ 0 };

  int m_numOfRegs{ 0 };
  int m_scanRate{ 0 };
  std::unique_ptr<QTimer> m_pollTimer{ nullptr };
  int m_packets{ 0 };
  int m_errors{ 0 };
  int m_timeOut{ 0 };
  bool m_transactionIsPending{ false };
  bool m_readOutputsBeforeWrite{ true };
  gsl::owner<uint8_t*> m_dest{ nullptr };
  gsl::owner<uint16_t*> m_dest16{ nullptr };

signals:
  void refreshView();

public slots:
  void modbusTransaction();
  void resetCounters();
};

#endif // MODBUSADAPTER_H
