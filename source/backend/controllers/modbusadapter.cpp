#include "modbusadapter.h"

#include "screens/mainwindow.h"
#include <QApplication>
#include <cerrno>
#include <modbus-rtu.h>

ModbusAdapter* m_instance;

ModbusAdapter::ModbusAdapter(QObject* parent)
  : QObject{ parent }
  , m_modbus{ nullptr }
  , regModel{ std::make_unique<RegistersModel>(this) }
  , rawModel{ std::make_unique<RawDataModel>(this) }
  , m_pollTimer{ std::make_unique<QTimer>(this) }
  , m_dest{ static_cast<uint8_t*>(std::malloc(2000 * sizeof(uint8_t))) }
  , m_dest16{ static_cast<uint16_t*>(std::malloc(125 * sizeof(uint16_t))) }
{
  m_instance = this;

  connect(m_pollTimer.get(),
          &QTimer::timeout,
          this,
          &ModbusAdapter::modbusTransaction);
  connect(regModel.get(),
          &RegistersModel::refreshView,
          this,
          &ModbusAdapter::refreshView);

  // setup memory for data
  std::memset(m_dest, 0, 2000 * sizeof(uint8_t));
  std::memset(m_dest16, 0, 125 * sizeof(uint16_t));
}

ModbusAdapter::~ModbusAdapter()
{
  free(m_dest);
  free(m_dest16);
}

void
ModbusAdapter::modbusConnectRTU(const QString& port,
                                int baud,
                                QChar parity,
                                int data_bits,
                                int stop_bits,
                                int rts,
                                int timeOut)
{
  // Modbus RTU connect

  modbusDisConnect();

  qInfo() << "Modbus Connect RTU";

  m_modbus = modbus_new_rtu(
    port.toLatin1().constData(), baud, parity.toLatin1(), data_bits, stop_bits);
  auto line{ QStringLiteral("Connecting to Serial Port [ %1 ]...").arg(port) };
  qDebug() << line;

// Debug messages from libmodbus
#ifdef LIB_MODBUS_DEBUG_OUTPUT
  modbus_set_debug(m_modbus, 1);
#endif

  m_timeOut = timeOut;

  if (m_modbus == nullptr) {
    main_win->showUpInfoBar(tr("Unable to create the libmodbus context."),
                            InfoBar::Error);
    qWarning() << "Connection failed. Unable to create the libmodbus context";
    return;
  }
  if (m_modbus && modbus_set_slave(m_modbus, m_slave) == -1) {
    modbus_free(m_modbus);
    main_win->showUpInfoBar(tr("Invalid slave ID."), InfoBar::Error);
    qWarning() << "Connection failed. Invalid slave ID";
    return;
  } else if (m_modbus && modbus_connect(m_modbus) == -1) {
    modbus_free(m_modbus);
    main_win->showUpInfoBar(
      tr("Connection failed\nCould not connect to serial port."),
      InfoBar::Error);
    qWarning() << "Connection failed. Could not connect to serial port";
    m_connected = false;
    line += "Failed";
  } else {
    // error recovery mode
    modbus_set_error_recovery(m_modbus, MODBUS_ERROR_RECOVERY_PROTOCOL);
    // response_timeout;
    modbus_set_response_timeout(m_modbus, timeOut, 0);
    m_connected = true;
    line += "OK";
    main_win->hideInfoBar();
    qDebug() << line;
  }

  m_modBusMode = EUtils::RTU;

  // Add line to raw data model
  line = EUtils::sysTimeStamp() + " - " + line;
  rawModel->addLine(line);
}

void
ModbusAdapter::modbusConnectTCP(const QString &ip, int port, int timeOut)
{
  // Modbus TCP connect
  QString stripped_ip = "";

  modbusDisConnect();

  qInfo() << "Modbus Connect TCP";

  auto line{ QStringLiteral("Connecting to IP : %1:%2").arg(ip, port) };
  qDebug() << line;

  stripped_ip = stripIP(ip);
  if (stripped_ip == "") {
    main_win->showUpInfoBar(tr("Connection failed\nBlank IP Address."),
                            InfoBar::Error);
    qWarning() << "Connection failed. Blank IP Address";
    return;
  }
  m_modbus = modbus_new_tcp(stripped_ip.toLatin1().constData(), port);
  main_win->hideInfoBar();
  qDebug() << "Connecting to IP : " << ip << ":" << port;

// Debug messages from libmodbus
#ifdef LIB_MODBUS_DEBUG_OUTPUT
  modbus_set_debug(m_modbus, 1);
#endif

  m_timeOut = timeOut;

  if (m_modbus == nullptr) {
    main_win->showUpInfoBar(tr("Unable to create the libmodbus context."),
                            InfoBar::Error);
    qWarning() << "Connection failed. Unable to create the libmodbus context";
    return;
  }
  if (m_modbus && modbus_connect(m_modbus) == -1) {
    modbus_free(m_modbus);
    main_win->showUpInfoBar(
      tr("Connection failed\nCould not connect to TCP port."), InfoBar::Error);
    qWarning() << "Connection to IP : " << ip << ":" << port
               << "...failed. Could not connect to TCP port";
    m_connected = false;
    line += " Failed";
  } else {
    // error recovery mode
    modbus_set_error_recovery(m_modbus, MODBUS_ERROR_RECOVERY_PROTOCOL);
    // response_timeout;
    modbus_set_response_timeout(m_modbus, timeOut, 0);
    m_connected = true;
    line += " OK";
    main_win->hideInfoBar();
    qDebug() << line;
  }

  m_modBusMode = EUtils::TCP;

  // Add line to raw data model
  line = EUtils::sysTimeStamp() + " - " + line;
  rawModel->addLine(line);
}

void
ModbusAdapter::modbusDisConnect()
{
  // Modbus disconnect

  qInfo() << "Modbus disconnected";

  if (m_modbus != nullptr) {
    if (m_connected) {
      modbus_close(m_modbus);
      modbus_free(m_modbus);
    }
    m_modbus = nullptr;
  }

  m_connected = false;

  m_modBusMode = EUtils::None;
}

void
ModbusAdapter::modbusTransaction()
{
  // Modbus request data

  qInfo() << "Modbus Transaction. Function Code = " << m_functionCode;
  m_packets += 1;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  switch (m_functionCode) {
    case MODBUS_FC_READ_COILS:
    case MODBUS_FC_READ_DISCRETE_INPUTS:
    case MODBUS_FC_READ_HOLDING_REGISTERS:
    case MODBUS_FC_READ_INPUT_REGISTERS:
      modbusReadData(m_slave, m_functionCode, m_startAddr, m_numOfRegs);
      break;

    case MODBUS_FC_WRITE_SINGLE_COIL:
    case MODBUS_FC_WRITE_SINGLE_REGISTER:
    case MODBUS_FC_WRITE_MULTIPLE_COILS:
    case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
      modbusWriteData(m_slave, m_functionCode, m_startAddr, m_numOfRegs);
      break;
    default:
      break;
  }

  QApplication::setOverrideCursor(Qt::ArrowCursor);

  emit(refreshView());
}

void
ModbusAdapter::modbusReadData(int slave,
                              int function_code,
                              int start_address,
                              int no_of_items)
{

  qInfo() << "Modbus Read Data ";

  if (m_modbus == nullptr) {
    return;
  }

  int ret       = -1; // return value from read functions
  bool is16_bit = false;

  modbus_set_slave(m_modbus, slave);
  // request data from modbus
  switch (function_code) {
    case MODBUS_FC_READ_COILS:
      ret = modbus_read_bits(m_modbus, start_address, no_of_items, m_dest);
      break;

    case MODBUS_FC_READ_DISCRETE_INPUTS:
      ret =
        modbus_read_input_bits(m_modbus, start_address, no_of_items, m_dest);
      break;

    case MODBUS_FC_READ_HOLDING_REGISTERS:
      ret =
        modbus_read_registers(m_modbus, start_address, no_of_items, m_dest16);
      is16_bit = true;
      break;

    case MODBUS_FC_READ_INPUT_REGISTERS:
      ret = modbus_read_input_registers(
        m_modbus, start_address, no_of_items, m_dest16);
      is16_bit = true;
      break;

    default:
      break;
  }

  qDebug() << "Modbus Read Data return value = " << ret
           << ", errno = " << errno;

  // update data model
  if (ret == no_of_items) {
    if (regModel->getFrmt() != EUtils::Float) {
      for (int i = 0; i < no_of_items; ++i) {
        int data = is16_bit ? m_dest16[i] : m_dest[i];
        regModel->setValue(i, data);
      }
    } else // read float values
    {
      for (int i = 0; i < no_of_items - 1; i += 2) {
        int data_hi = is16_bit ? m_dest16[i] : m_dest[i];
        int data_lo = is16_bit ? m_dest16[i + 1] : m_dest[i + 1];
        regModel->setValue32(i / 2, data_hi, data_lo);
      }
    }

    main_win->hideInfoBar();
  } else {

    regModel->setNoValidValues();
    m_errors += 1;

    QString line = "";
    if (ret < 0) {
      line = QString("Error : ") + EUtils::libmodbus_strerror(errno);
      qWarning() << "Read Data failed. " << line;
      rawModel->addLine(EUtils::sysTimeStamp() + " - " + line);
      line = QString(tr("Read data failed.\nError : ")) +
             EUtils::libmodbus_strerror(errno);
    } else {
      line = QString("Number of registers returned does not match number of "
                     "registers requested!. Error : ") +
             EUtils::libmodbus_strerror(errno);
      qWarning() << "Read Data failed. " << line;
      rawModel->addLine(EUtils::sysTimeStamp() + " - " + line);
      line = QString(tr("Read data failed.\nNumber of registers returned does "
                        "not match number of registers requested!. Error : ")) +
             EUtils::libmodbus_strerror(errno);
    }

    main_win->showUpInfoBar(line, InfoBar::Error);
    modbus_flush(m_modbus); // flush data
  }
}

void
ModbusAdapter::modbusWriteData(int slave,
                               int function_code,
                               int startAddress,
                               int noOfItems)
{

  qInfo() << "Modbus Write Data ";

  if (m_modbus == nullptr) {
    return;
  }

  int ret = -1; // return value from functions

  union
  {
    struct
    {
      qint16 low, high;
    } reg;
    float value;
  } modelData{};

  modbus_set_slave(m_modbus, slave);
  // request data from modbus
  switch (function_code) {
    case MODBUS_FC_WRITE_SINGLE_COIL:
      ret       = modbus_write_bit(m_modbus, startAddress, regModel->value(0));
      noOfItems = 1;
      break;

    case MODBUS_FC_WRITE_SINGLE_REGISTER:
      ret = modbus_write_register(m_modbus, startAddress, regModel->value(0));
      noOfItems = 1;
      break;

    case MODBUS_FC_WRITE_MULTIPLE_COILS: {
      auto* data = new uint8_t[noOfItems];
      for (int i = 0; i < noOfItems; ++i) {
        data[i] = regModel->value(i);
      }
      ret = modbus_write_bits(m_modbus, startAddress, noOfItems, data);
      delete[] data;
      break;
    }
    case MODBUS_FC_WRITE_MULTIPLE_REGISTERS: {
      if (regModel->getFrmt() != EUtils::Float) {
        auto* data = new uint16_t[noOfItems];
        for (int i = 0; i < noOfItems; ++i) {
          data[i] = regModel->value(i);
        }
        ret = modbus_write_registers(m_modbus, startAddress, noOfItems, data);
        delete[] data;
        break;
      } // write float values
      uint16_t* data = new uint16_t[noOfItems];
      for (int i = 0; i < noOfItems - 1; i += 2) {
        modelData.value = regModel->floatValue(i / 2);
        if (regModel->getEndian() == EUtils::Little) {
          data[i]     = modelData.reg.low;
          data[i + 1] = modelData.reg.high;
        } else if (regModel->getEndian() == EUtils::Big) {
          data[i]     = modelData.reg.high;
          data[i + 1] = modelData.reg.low;
        }
      }
      ret = modbus_write_registers(m_modbus, startAddress, noOfItems, data);
      delete[] data;
      break;
    }

    default:
      break;
  }

  qDebug() << "Modbus Write Data return value = " << ret
           << ", errno = " << errno;

  // update data model
  if (ret == noOfItems) {
    // values written correctly
    rawModel->addLine(EUtils::sysTimeStamp() + " - values written correctly.");
    main_win->hideInfoBar();
  } else {

    regModel->setNoValidValues();
    m_errors += 1;

    QString line;
    if (ret < 0) {
      line = QString("Error : ") + EUtils::libmodbus_strerror(errno);
      qWarning() << "Write Data failed. " << line;
      rawModel->addLine(EUtils::sysTimeStamp() + " - " + line);
      line = QString(tr("Write data failed.\nError : ")) +
             EUtils::libmodbus_strerror(errno);
    } else {
      line = QString("Number of registers returned does not match number of "
                     "registers requested!. Error : ") +
             EUtils::libmodbus_strerror(errno);
      qWarning() << "Write Data failed. " << line;
      rawModel->addLine(EUtils::sysTimeStamp() + " - " + line);
      line = QString(tr("Write data failed.\nNumber of registers returned does "
                        "not match number of registers requested!. Error : ")) +
             EUtils::libmodbus_strerror(errno);
    }

    main_win->showUpInfoBar(line, InfoBar::Error);
    modbus_flush(m_modbus); // flush data
  }
}

void
ModbusAdapter::busMonitorRequestData(uint8_t* data, int data_len)
{
  // Request Raw data from port - Update raw data model

  QString line;

  for (int i = 0; i < data_len; ++i) {
    line += QString().asprintf("%.2x  ", data[i]);
  }

  qInfo() << "Tx Data : " << line;
  line = EUtils::txTimeStamp(m_modBusMode) + " - " + line.toUpper();

  rawModel->addLine(line);

  m_transactionIsPending = true;
}
void
ModbusAdapter::busMonitorResponseData(uint8_t* data, int data_len)
{

  // Response Raw data from port - Update raw data model

  QString line;

  for (int i = 0; i < data_len; ++i) {
    line += QString().asprintf("%.2x  ", data[i]);
  }

  qInfo() << "Rx Data : " << line;
  line = EUtils::rxTimeStamp(m_modBusMode) + " - " + line.toUpper();

  rawModel->addLine(line);

  m_transactionIsPending = false;
}

void
ModbusAdapter::addItems()
{
  // TODO - fix base addr
  regModel->addItems(m_startAddr + m_baseAddr,
                     m_numOfRegs,
                     EUtils::modbusIsWriteFunction(m_functionCode));
  // If it is a write function -> read registers
  if (!m_connected || !m_readOutputsBeforeWrite) {
    return;
  }
  if (EUtils::modbusIsWriteCoilsFunction(m_functionCode)) {
    modbusReadData(m_slave, EUtils::ReadCoils, m_startAddr, m_numOfRegs);
    emit(refreshView());
  } else if (EUtils::modbusIsWriteRegistersFunction(m_functionCode)) {
    modbusReadData(m_slave, EUtils::ReadHoldRegs, m_startAddr, m_numOfRegs);
    emit(refreshView());
  }
}

void
ModbusAdapter::setScanRate(int scan_rate)
{
  m_scanRate = scan_rate;
}

void
ModbusAdapter::resetCounters()
{
  m_packets = 0;
  m_errors  = 0;
  emit(refreshView());
}

void
ModbusAdapter::startPollTimer()
{
  m_pollTimer->start(m_scanRate);
}

void
ModbusAdapter::stopPollTimer()
{
  m_pollTimer->stop();
}

QString
ModbusAdapter::stripIP(const QString &ip)
{
  // Strip zero's from IP
  QStringList ip_bytes;
  QString res = "";
  int i       = 0;

  ip_bytes = ip.split(".");
  if (ip_bytes.size() == 4) {
    res = QString("").setNum(ip_bytes[0].toInt());
    i   = 1;
    while (i < ip_bytes.size()) {
      res = res + "." + QString("").setNum(ip_bytes[i].toInt());
      i++;
    }
    return res;
  }
  return "";
}

void
ModbusAdapter::setReadOutputsBeforeWrite(bool read_outputs_before_write)
{

  m_readOutputsBeforeWrite = read_outputs_before_write;
}

extern "C"
{

  void bus_monitor_raw_response_data(uint8_t* data, int data_len)
  {
    m_instance->busMonitorResponseData(data, data_len);
  }

  void bus_monitor_raw_request_data(uint8_t* data, int data_len)
  {
    m_instance->busMonitorRequestData(data, data_len);
  }
}
