#ifndef MODBUSCOMMSETTINGS_H
#define MODBUSCOMMSETTINGS_H

#include <QSettings>

class ModbusCommSettings : public QSettings
{
  Q_OBJECT
public:
  explicit ModbusCommSettings(QObject* parent = nullptr)
    : QSettings(parent)
  {
  }
  ModbusCommSettings(const QString& file_name,
                     Format format   = QSettings::IniFormat,
                     QObject* parent = nullptr);
  // TCP
  [[nodiscard]] auto tcpPort() const { return m_tcpPort; }
  void setTCPPort(const QString& tcp_port) { m_tcpPort = tcp_port; }
  void setSlaveIP(const QString& ip) { m_slaveIP = ip; }
  [[nodiscard]] auto slaveIP() const { return m_slaveIP; }
  // Serial
  [[nodiscard]] auto serialDev() const { return m_serialDev; }
  [[nodiscard]] auto serialPort() const { return m_serialPort; }
  [[nodiscard]] auto serialPortName() const { return m_serialPortName; }
  void setSerialPort(const QString& serial_port,
                     const QString& serial_dev = {});
  [[nodiscard]] auto baud() const { return m_baud; }
  void setBaud(const QString& baud) { m_baud = baud; }
  [[nodiscard]] auto dataBits() const { return m_dataBits; }
  void setDataBits(const QString& data_bits) { m_dataBits = data_bits; }
  [[nodiscard]] auto stopBits() const { return m_stopBits; }
  void setStopBits(const QString& stop_bits) { m_stopBits = stop_bits; }
  [[nodiscard]] auto parity() const { return m_parity; }
  void setParity(const QString& parity) { m_parity = parity; }
  [[nodiscard]] auto rts() const { return m_rts; }
  void setRTS(const QString& t_rts) { m_rts = t_rts; }
  // Var
  [[nodiscard]] auto maxNoOfLines() const { return m_maxNoOfLines; }
  void setMaxNoOfLines(const QString& max_no_of_lines)
  {
    m_maxNoOfLines = max_no_of_lines;
  }
  [[nodiscard]] auto baseAddr() const { return m_baseAddr; }
  void setBaseAddr(const QString& base_addr) { m_baseAddr = base_addr; }
  [[nodiscard]] auto timeOut() const { return m_timeOut; }
  void setTimeOut(const QString& time_out) { m_timeOut = time_out; }
  [[nodiscard]] auto endian() const { return m_endian; }
  void setEndian(int endian) { m_endian = endian; }
  void loadSettings();
  void saveSettings();
  // logging
  [[nodiscard]] auto loggingLevel() const { return m_loggingLevel; }
  //
  [[nodiscard]] auto readOutputsBeforeWrite() const
  {
    return m_readOutputsBeforeWrite;
  }
  // session
  [[nodiscard]] auto modbusMode() const { return m_modbusMode; }
  void setModbusMode(int modbus_mode) { m_modbusMode = modbus_mode; }
  [[nodiscard]] auto slaveID() const { return m_slaveID; }
  void setSlaveID(int slave_id) { m_slaveID = slave_id; }
  [[nodiscard]] auto scanRate() const { return m_scanRate; }
  void setScanRate(int scan_rate) { m_scanRate = scan_rate; }
  [[nodiscard]] auto functionCode() const { return m_functionCode; }
  void setFunctionCode(int function_code) { m_functionCode = function_code; }
  [[nodiscard]] auto startAddr() const { return m_startAddr; }
  void setStartAddr(int start_addr) { m_startAddr = start_addr; }
  [[nodiscard]] auto noOfRegs() const { return m_noOfRegs; }
  void setNoOfRegs(int no_of_regs) { m_noOfRegs = no_of_regs; }
  [[nodiscard]] auto frmt() const { return m_frmt; }
  void setFrmt(int frmt) { m_frmt = frmt; }
  [[nodiscard]] auto floatPrecision() const { return m_floatPrecision; }
  void setfloatPrecision(int precision) { m_floatPrecision = precision; }
  void loadSession(const QString& f_name);
  void saveSession(const QString& f_name);

private:
  // TCP
  QString m_tcpPort;
  QString m_slaveIP;
  // Serial
  QString m_serialDev;
  QString m_serialPort;
  QString m_serialPortName;
  QString m_baud;
  QString m_dataBits;
  QString m_stopBits;
  QString m_parity;
  QString m_rts;
  // Var
  QString m_maxNoOfLines;
  QString m_baseAddr;
  QString m_timeOut;
  int m_endian{ 0 };
  void load(QSettings* s);
  void save(QSettings* s);
  // Log
  int m_loggingLevel{ 0 };
  // read outputs before write
  bool m_readOutputsBeforeWrite{ false };
  // Session vars
  int m_modbusMode{ 0 };
  int m_slaveID{ 0 };
  int m_scanRate{ 0 };
  int m_functionCode{ 0 };
  int m_startAddr{ 0 };
  int m_noOfRegs{ 0 };
  int m_frmt{ 0 };
  int m_floatPrecision{ 0 };

signals:

public slots:
};

#endif // MODBUSCOMMSETTINGS_H
