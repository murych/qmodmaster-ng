#ifndef REGISTERSMODEL_H
#define REGISTERSMODEL_H

#include "views/registersdatadelegate.h"
#include <QObject>
#include <QStandardItemModel>

static const QString REG_MODEL_HEADER_LABELS[] = { "+00", "+01", "+02", "+03",
                                                   "+04", "+05", "+06", "+07",
                                                   "+08", "+09" };
static const QString REG_MODEL_FLOAT_HEADER_LABELS[] = {
  "+00", "+02", "+04", "+06", "+08", "+10", "+12", "+14", "+16", "+18"
};
static const int ADDRESS_COLUMN = 0;
static const int VALUE_COLUMN   = 1;

class RegistersModel : public QObject
{
  Q_OBJECT
public:
  explicit RegistersModel(QObject* parent = nullptr);
  ~RegistersModel() override;

  void addItems(int start_address, int no_of_items, bool value_is_editable);
  void setValue(int idx, int value) const;
  void setValue32(int idx, int value_hi, int value_lo) const;
  void setFrmt(int frmt);
  [[nodiscard]] auto getFrmt() const { return m_frmt; }
  void setStartAddrBase(int base);
  void setIs16Bit(bool is16_bit);
  void setIsSigned(bool is_signed);
  void setEndian(int endian) { m_endian = endian; }
  [[nodiscard]] auto getEndian() const { return m_endian; }
  void setFloatPrecision(int precision);
  [[nodiscard]] auto strValue(int idx) const -> QString;
  int value(int idx);
  float floatValue(int idx);
  std::unique_ptr<QStandardItemModel> model{ nullptr };
  void clear() const;
  void setNoValidValues() const;
  [[nodiscard]] auto* itemDelegate() const { return m_regDataDelegate.get(); }

private:
  void changeFrmt(int frmt);
  int m_startAddress{};
  int m_noOfItems{ 0 };
  int m_firstRow{};
  int m_lastRow{};
  bool m_is16Bit{ false };
  bool m_isSigned{ false };
  int m_startAddrBase{ 10 };
  int m_frmt{};
  int m_endian{};
  int m_floatPrecision{};
  std::unique_ptr<RegistersDataDelegate> m_regDataDelegate{ nullptr };

signals:
  void refreshView();

public slots:
};

#endif // REGISTERSMODEL_H
