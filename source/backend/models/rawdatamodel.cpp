#include "rawdatamodel.h"

RawDataModel::RawDataModel(QObject* parent)
  : QObject{ parent }
  , model{ std::make_unique<QStringListModel>(this) }
{
}

RawDataModel::~RawDataModel() = default;

void
RawDataModel::addLine(const QString& line)
{
  if (!m_addLinesEnabled) {
    return;
  }

  qDebug() << "Raw Data Model Line = " << line
               << " , No of lines = " << m_rawData.length();

  if (m_rawData.length() == m_maxNoOfLines) {
    m_rawData.removeFirst();
  }
  m_rawData.append(line);
  model->setStringList(m_rawData);
}

void
RawDataModel::clear()
{
  qDebug() << "Raw Data Model cleared";

  m_rawData.clear();
  model->setStringList(m_rawData);
}
