#include "registersmodel.h"

#include "utils/eutils.h"
#include <QStandardItem>
#include <cmath>

RegistersModel::RegistersModel(QObject* parent)
  : QObject(parent)
  , model{ std::make_unique<QStandardItemModel>(0, 0, this) }
  , m_regDataDelegate{ std::make_unique<RegistersDataDelegate>(this) }
{
  clear();
}

RegistersModel::~RegistersModel() = default;

void
RegistersModel::addItems(int start_address,
                         int no_of_items,
                         bool value_is_editable)
{
  int row        = 0;
  int col        = 0;
  m_startAddress = start_address;
  m_noOfItems    = no_of_items;

  qDebug() << "Registers Model Address = " << start_address
               << " , noOfItems = " << no_of_items
               << " , first row = " << m_firstRow
               << " , last row = " << m_lastRow;

  // Format Vertical - Horizontal Headers
  clear();
  if (no_of_items > 1) {
    if (m_frmt == EUtils::Float) {
      m_firstRow = 0;
      m_lastRow  = (no_of_items - 1) / 20;
      model->setHorizontalHeaderLabels(QStringList()
                                       << REG_MODEL_FLOAT_HEADER_LABELS[0]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[1]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[2]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[3]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[4]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[5]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[6]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[7]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[8]
                                       << REG_MODEL_FLOAT_HEADER_LABELS[9]);

      QStringList vert_header;
      for (int i = m_firstRow; i <= m_lastRow; i++) {
        vert_header << QString("%1").arg(
          start_address + i * 20, 2, 10, QLatin1Char('0'));
      }
      model->setVerticalHeaderLabels(vert_header);
    } else {
      m_firstRow = 0;
      m_lastRow  = (no_of_items - 1) / 10;
      model->setHorizontalHeaderLabels(
        QStringList()
        << REG_MODEL_HEADER_LABELS[0] << REG_MODEL_HEADER_LABELS[1]
        << REG_MODEL_HEADER_LABELS[2] << REG_MODEL_HEADER_LABELS[3]
        << REG_MODEL_HEADER_LABELS[4] << REG_MODEL_HEADER_LABELS[5]
        << REG_MODEL_HEADER_LABELS[6] << REG_MODEL_HEADER_LABELS[7]
        << REG_MODEL_HEADER_LABELS[8] << REG_MODEL_HEADER_LABELS[9]);

      QStringList vert_header;
      for (int i = m_firstRow; i <= m_lastRow; i++) {
        vert_header << QString("%1").arg(
          start_address + i * 10, 2, 10, QLatin1Char('0'));
      }
      model->setVerticalHeaderLabels(vert_header);
    }
  } else {
    model->setHorizontalHeaderLabels(QStringList()
                                     << REG_MODEL_HEADER_LABELS[0]);
    model->setVerticalHeaderLabels(QStringList() << QString("%1").arg(
                                     start_address, 2, 10, QLatin1Char('0')));
  }

  // Add data to model
  if (no_of_items == 1) {
    auto* value_item = new QStandardItem("-");
    model->setItem(0, 0, value_item);
    value_item->setEditable(value_is_editable);
  } else {
    if (m_frmt == EUtils::Float) {
      m_firstRow = 0;
      m_lastRow  = (no_of_items - 1) / 20;
      for (int i = m_firstRow; i <= m_lastRow; i++) {
        row = i;
        for (int j = 0; j < 10; j++) {
          col = j;
          if ((row * 10 + col) >= (no_of_items / 2)) { // not used cells
            auto* value_item = new QStandardItem("x");
            model->setItem(row, col, value_item);
            value_item->setEditable(false);
            value_item->setForeground(QBrush(Qt::red));
            value_item->setBackground(QBrush(Qt::lightGray));
          } else {
            auto* value_item = new QStandardItem("-");
            model->setItem(row, col, value_item);
            value_item->setEditable(value_is_editable);
          }
        }
      }
    } else {
      m_firstRow = 0;
      m_lastRow  = (no_of_items - 1) / 10;
      for (int i = m_firstRow; i <= m_lastRow; i++) {
        row = i;
        for (int j = 0; j < 10; j++) {
          col = j;
          if ((row * 10 + col) >= (no_of_items)) { // not used cells
            auto* value_item = new QStandardItem("x");
            model->setItem(row, col, value_item);
            value_item->setEditable(false);
            value_item->setForeground(QBrush(Qt::red));
            value_item->setBackground(QBrush(Qt::lightGray));
          } else {
            auto* value_item = new QStandardItem("-");
            model->setItem(row, col, value_item);
            value_item->setEditable(value_is_editable);
          }
        }
      }
    }
  }

  emit(refreshView());
}

void
RegistersModel::setNoValidValues() const
{
  int row = 0;
  int col = 0;
  // if we have no valid values we set  as value = '-/-'

  if (m_frmt == EUtils::Float) {
    for (int i = 0; i < (m_noOfItems / 2); i++) {
      row               = i / 10;
      col               = i % 10;
      QModelIndex index = model->index(row, col, QModelIndex());
      model->setData(index, QBrush(Qt::red), Qt::ForegroundRole);
      model->setData(index, "-/-", Qt::DisplayRole);
    }
  } else {
    for (int i = 0; i < m_noOfItems; i++) {
      row               = i / 10;
      col               = i % 10;
      QModelIndex index = model->index(row, col, QModelIndex());
      model->setData(index, QBrush(Qt::red), Qt::ForegroundRole);
      model->setData(index, "-/-", Qt::DisplayRole);
    }
  }
}

void
RegistersModel::setValue(int idx, int value) const
{
  auto row{ 0 };
  auto col{ 0 };
  auto converted_value{ EUtils::formatValue(
    value, m_frmt, m_is16Bit, m_isSigned) };

  // set model data
  if (m_noOfItems == 1) {
    row = 0;
    col = 0;
  } else {
    row = (idx) / 10;
    col = (idx) % 10;
  }

  auto index{ model->index(row, col, QModelIndex{}) };
  model->setData(index, QBrush{ Qt::black }, Qt::ForegroundRole);
  model->setData(index, converted_value, Qt::DisplayRole);
  model->setData(index,
                 QStringLiteral("Address : %1")
                   .arg(m_startAddress + idx, 1, m_startAddrBase)
                   .toUpper(),
                 Qt::ToolTipRole);
}

void
RegistersModel::setValue32(int idx, int value_hi, int value_lo) const
{ // update model with float values
  auto row{ 0 };
  auto col{ 0 };
  auto converted_value{ EUtils::formatValue32(
    value_hi, value_lo, m_endian, m_floatPrecision) };

  // set model data
  if (m_noOfItems == 1) {
    row = 0;
    col = 0;
  } else {
    row = (idx) / 10;
    col = (idx) % 10;
  }

  auto index{ model->index(row, col, QModelIndex{}) };
  model->setData(index, QBrush(Qt::black), Qt::ForegroundRole);
  model->setData(index, converted_value, Qt::DisplayRole);
  model->setData(index,
                 QString("Address : %1")
                   .arg(m_startAddress + 2 * idx, 1, m_startAddrBase)
                   .toUpper(),
                 Qt::ToolTipRole);
}

int
RegistersModel::value(int idx)
{
  auto ok{ false };

  // Get Value

  const auto string_val{ strValue(idx) };
  const auto int_val{ string_val.toInt(&ok, m_frmt) };
  if (ok) {
    return int_val;
  }
  return -1;
}

float
RegistersModel::floatValue(int idx)
{
  //  QString string_val;
  //  float float_val = NAN;
  auto ok{ false };

  // Get Value
  const auto string_val{ strValue(idx) };
  const auto float_val{ string_val.toFloat(&ok) };
  if (ok) {
    return float_val;
  }
  return -1;
}

QString
RegistersModel::strValue(int idx) const
{
  int row = 0;
  int col = 0;

  // get model data
  if (m_noOfItems == 1) {
    row = 0;
    col = 0;
  } else {
    row = (idx) / 10;
    col = (idx) % 10;
  }
  auto index{ model->index(row, col, QModelIndex{}) };
  auto value{ model->data(index, Qt::DisplayRole) };
  if (value.canConvert<QString>()) {
    return value.toString();
  }
  return "-/-";
}

void
RegistersModel::changeFrmt(int frmt)
{

  QString string_val;
  int int_val = 0;
  int row     = 0;
  int col     = 0;
  bool ok     = false;
  QString converted_val;

  qDebug() << "Registers Model changed format from " << m_frmt << " to "
               << frmt;

  // change base
  for (int idx = 0; idx < m_noOfItems; idx++) {
    // Get Value
    string_val = strValue(idx);
    int_val    = string_val.toInt(&ok, m_frmt);
    // Format Value
    if (ok) {
      converted_val = EUtils::formatValue(int_val, frmt, m_is16Bit, m_isSigned);
    } else {
      converted_val = "-/-";
    }
    // Update
    if (m_noOfItems == 1) {
      row = 0;
      col = 0;
    } else {
      row = (idx) / 10;
      col = (idx) % 10;
    }
    auto index{ model->index(row, col, QModelIndex{}) };
    model->setData(index, converted_val, Qt::DisplayRole);
    model->setData(index,
                   QString("Address : %1")
                     .arg(m_startAddress + idx, m_startAddrBase)
                     .toUpper(),
                   Qt::ToolTipRole);
  }

  emit(refreshView());
}

void
RegistersModel::clear() const
{
  qDebug() << "Registers Model Cleared";

  // Clear model
  model->clear();
}

void
RegistersModel::setStartAddrBase(int base)
{
  qDebug() << "Registers Model start addr set base = " << base;

  m_startAddrBase = base;
  changeFrmt(m_frmt);
}

void
RegistersModel::setFrmt(int frmt)
{
  qDebug() << "Registers Model set base = " << frmt;

  m_regDataDelegate->setFrmt(frmt);
  changeFrmt(frmt);
  m_frmt = frmt;
}

void
RegistersModel::setIs16Bit(bool is16_bit)
{
  qDebug() << "Registers Model Is16Bit = " << is16_bit;
  m_is16Bit = is16_bit;
  m_regDataDelegate->setIs16Bit(is16_bit);
}

void
RegistersModel::setIsSigned(bool is_signed)
{
  qDebug() << "Registers Model IsSigned = " << is_signed;
  m_isSigned = is_signed;
  m_regDataDelegate->setIsSigned(is_signed);
  changeFrmt(m_frmt);
}

void
RegistersModel::setFloatPrecision(int precision)
{
  qDebug() << "Registers Model float precision = " << precision;
  m_floatPrecision = precision;
  m_regDataDelegate->setFloatPrecision(precision);
}
