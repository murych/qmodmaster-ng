#ifndef RAWDATAMODEL_H
#define RAWDATAMODEL_H

#include <QObject>
#include <QStringListModel>

class RawDataModel : public QObject
{
  Q_OBJECT
public:
  explicit RawDataModel(QObject* parent = nullptr);
  ~RawDataModel() override;

  std::unique_ptr<QStringListModel> model{ nullptr };
  void addLine(const QString& line);
  void enableAddLines(bool en) { m_addLinesEnabled = en; }
  void clear();
  void setMaxNoOfLines(int no_of_lines) { m_maxNoOfLines = no_of_lines; }
  [[nodiscard]] auto maxNoOfLines() const { return m_maxNoOfLines; }

signals:

public slots:

private:
  QStringList m_rawData;
  int m_maxNoOfLines{ 0 };
  bool m_addLinesEnabled{ false };
};

#endif // RAWDATAMODEL_H
