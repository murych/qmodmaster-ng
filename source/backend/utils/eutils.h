#ifndef EUTILS_H
#define EUTILS_H

#include <QMap>
#include <QString>
#include <QTime>
#include <modbus.h>

static const QString MODBUS_FUNCTION_NAMES[] = {
  "Read Coils (0x01)",
  "Read Discrete Inputs (0x02)",
  "Read Holding Registers (0x03)",
  "Read Input Registers (0x04)",
  "Write Single Coil (0x05)",
  "Write Single Register (0x06)",
  "Write Multiple Coils (0x0f)",
  "Write Multiple Registers (0x10)",
  "Report Server ID (0x11)"
};
static const int MODBUS_FUNCTION_CODES[] = { 0x1, 0x2, 0x3,  0x4, 0x5,
                                             0x6, 0xf, 0x10, 0x11 };
static const QString MODBUS_MODE_STAMP[] = { "[RTU]>", "[TCP]>", "" };

class EUtils
{

  EUtils();

public:
  static auto modbusDataTypeName(int f_code)
  {
    switch (f_code) {
      case MODBUS_FC_READ_COILS:
      case MODBUS_FC_WRITE_SINGLE_COIL:
      case MODBUS_FC_WRITE_MULTIPLE_COILS:
        return QStringLiteral("Coil (binary)");
      case MODBUS_FC_READ_DISCRETE_INPUTS:
        return QStringLiteral("Discrete Input (binary)");
      case MODBUS_FC_READ_HOLDING_REGISTERS:
      case MODBUS_FC_WRITE_SINGLE_REGISTER:
      case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        return QStringLiteral("Holding Register (16 bit)");
      case MODBUS_FC_READ_INPUT_REGISTERS:
        return QStringLiteral("Input Register (16 bit)");
      default:
        break;
    }
    return QStringLiteral("Unknown");
  }

  static auto modbusIsWriteFunction(int f_code)
  {
    switch (f_code) {
      case MODBUS_FC_READ_COILS:
      case MODBUS_FC_READ_DISCRETE_INPUTS:
      case MODBUS_FC_READ_HOLDING_REGISTERS:
      case MODBUS_FC_READ_INPUT_REGISTERS:
        return false;

      case MODBUS_FC_WRITE_SINGLE_COIL:
      case MODBUS_FC_WRITE_MULTIPLE_COILS:
      case MODBUS_FC_WRITE_SINGLE_REGISTER:
      case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        return true;

      default:
        break;
    }
    return false;
  }

  static auto modbusIsWriteCoilsFunction(int f_code)
  {
    switch (f_code) {
      case MODBUS_FC_READ_COILS:
      case MODBUS_FC_READ_DISCRETE_INPUTS:
      case MODBUS_FC_READ_HOLDING_REGISTERS:
      case MODBUS_FC_READ_INPUT_REGISTERS:
      case MODBUS_FC_WRITE_SINGLE_REGISTER:
      case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        return false;

      case MODBUS_FC_WRITE_SINGLE_COIL:
      case MODBUS_FC_WRITE_MULTIPLE_COILS:
        return true;

      default:
        break;
    }
    return false;
  }

  static auto modbusIsWriteRegistersFunction(int f_code)
  {
    switch (f_code) {
      case MODBUS_FC_READ_COILS:
      case MODBUS_FC_READ_DISCRETE_INPUTS:
      case MODBUS_FC_READ_HOLDING_REGISTERS:
      case MODBUS_FC_READ_INPUT_REGISTERS:
      case MODBUS_FC_WRITE_SINGLE_COIL:
      case MODBUS_FC_WRITE_MULTIPLE_COILS:
        return false;

      case MODBUS_FC_WRITE_SINGLE_REGISTER:
      case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        return true;

      default:
        break;
    }
    return false;
  }

  static auto modbusFunctionName(int index)
  {
    return MODBUS_FUNCTION_NAMES[index];
  }

  static auto modbusFunctionCode(int index)
  {
    return MODBUS_FUNCTION_CODES[index];
  }

  static auto txTimeStamp(int md)
  {
    return QStringLiteral("%1 Tx > %2")
      .arg(MODBUS_MODE_STAMP[md],
           QTime::currentTime().toString("HH:mm:ss:zzz"));
  }

  static auto rxTimeStamp(int md)
  {
    return QStringLiteral("%1 Rx > %2")
      .arg(MODBUS_MODE_STAMP[md],
           QTime::currentTime().toString("HH:mm:ss:zzz"));
  }

  static auto sysTimeStamp()
  {
    return QStringLiteral("Sys > %2")
      .arg(QTime::currentTime().toString("HH:mm:ss:zzz"));
  }

  static auto parity(const QString& p)
  {
    // the first char is what we need
    return p.at(0);
  }

  static auto endianness(int endian)
  {
    switch (endian) {
      case 0:
        return QStringLiteral("Little");
      case 1:
        return QStringLiteral("Big");
      default:
        return QStringLiteral("N/A");
    }
  }

  static enum { RTU = 0, TCP = 1, None = 0 } modbus_mode;

  static enum { Bin = 2, Dec = 10, Float = 11, Hex = 16 } number_format;
  static enum { Little = 0, Big = 1 } endianness_t;

  static enum {
    ReadCoils       = 0x1,
    ReadDisInputs   = 0x2,
    ReadHoldRegs    = 0x3,
    ReadInputRegs   = 0x4,
    WriteSingleCoil = 0x5,
    WriteSingleReg  = 0x6,
    WriteMultiCoils = 0xf,
    WriteMultiRegs  = 0x10
  } function_codes;

  static QString formatValue(int value,
                             int frmt,
                             bool is16_bit,
                             bool is_signed);

  static QString formatValue32(int value_hi,
                               int value_lo,
                               int endian,
                               int precision);
  static QString formatValue32(float value, int precision);

  static QString libmodbus_strerror(int errnum);
};

#endif // EUTILS_H
