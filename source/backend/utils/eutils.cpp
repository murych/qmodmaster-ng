#include "eutils.h"

#include <QDebug>

EUtils::EUtils() = default;

QString
EUtils::formatValue(int value, int frmt, bool is16_bit, bool is_signed = false)
{
  QString converted_value;

  switch (frmt) {

    case EUtils::Bin: // Binary
      if (is16_bit) {
        if (is_signed) {
          converted_value =
            QString("%1").arg((signed short)value, 16, 2, QLatin1Char('0'));
        } else {
          converted_value =
            QString("%1").arg((unsigned short)value, 16, 2, QLatin1Char('0'));
        }
      } else {

        converted_value = QString("%1").arg(value, 0, 2);
      }
      break;

    case EUtils::Dec: // Decimal - Unsigned Integer
      if (is_signed) {
        converted_value = QString("%1").arg((signed short)value, 0, 10);
      } else {
        converted_value = QString("%1").arg((unsigned short)value, 0, 10);
      }
      break;

    case EUtils::Hex: // Hex
      if (is16_bit) {
        converted_value = QString("%1").arg(value, 4, 16, QLatin1Char('0'));
      } else {
        converted_value = QString("%1").arg(value, 0, 16);
      }
      break;

    default: // Default
      converted_value = QString("%1").arg(value, 0, 10);
  }

  return converted_value.toUpper();
}

QString
EUtils::formatValue32(int value_hi,
                      int value_lo,
                      int endian    = EUtils::Little,
                      int precision = -1)
{
  union
  {
    struct
    {
      qint16 low, high;
    } reg;
    float value;
  } data{};
  QString converted_value;

  if (endian == EUtils::Little) {
    data.reg.high = value_lo;
    data.reg.low  = value_hi;
  } else if (endian == EUtils::Big) {
    data.reg.high = value_hi;
    data.reg.low  = value_lo;
  }

  converted_value = QString("%1").arg(data.value, 0, 'G', precision);

  return converted_value.toUpper();
}

QString
EUtils::formatValue32(float value, int precision = -1)
{
  return QStringLiteral("%1").arg(value, 0, 'G', precision).toUpper();
}

QString
EUtils::libmodbus_strerror(int errnum)
{
  switch (errnum) {
    case EINVAL:
      return "Protocol context is NULL";

    case ETIMEDOUT:
      return "Timeout";

    case ECONNRESET:
      return "Connection reset";

    case ECONNREFUSED:
      return "Connection refused";

    case EPIPE:
      return "Socket error";

    default: // Default
      return modbus_strerror(errno);
  }
}
